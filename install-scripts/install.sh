#!/bin/bash
SCRIPTPATH=$(cd `dirname "${BASH_SOURCE[0]}"` && pwd)
export FIREDRAKE_DIR=$SCRIPTPATH/firedrake-20150726

export PKG_CONFIG_PATH=$FIREDRAKE_DIR/lib/pkgconfig:$PKG_CONFIG_PATH
export PYTHONPATH=$FIREDRAKE_DIR/lib/python2.7/site-packages:$PYTHONPATH
export PATH=$FIREDRAKE_DIR/bin:$PATH
export PETSC_DIR=$FIREDRAKE_DIR/src/petsc
export SLEPC_DIR=$FIREDRAKE_DIR/src/slepc
export PETSC_ARCH=linux-gnu-c-opt

export FIREDRAKE_BRANCH=master
export PYOP2_BRANCH=master
export PETSC_BRANCH=master
export PETSC4PY_BRANCH=master

export LD_LIBRARY_PATH=$FIREDRAKE_DIR/usr/local/lib/python2.7/dist-packages/petsc/lib:$FIREDRAKE_DIR/src/petsc/$PETSC_ARCH/lib:$LD_LIBRARY_PATH
export PYTHONPATH=$FIREDRAKE_DIR/usr/local/lib/python2.7/dist-packages/:$PYTHONPATH
export INSTANT_CACHE_DIR=$FIREDRAKE_DIR/instant

SOURCE_DIR=$FIREDRAKE_DIR/src

# If the file is being sourced:
if [ "${BASH_SOURCE[0]}" != "${0}" ]
then
    echo "Setting paths for $FIREDRAKE_DIR"
    return
else
    # Download development versions
    if [ "$1" = "install" ]
    then
        mkdir --parents $SOURCE_DIR
        cd $SOURCE_DIR
        git clone git@bitbucket.org:petsc/petsc.git
        git clone git@bitbucket.org:slepc/slepc.git
        git clone https://github.com/cython/cython.git || true
        git clone git@bitbucket.org:petsc/petsc4py.git
        git clone git@bitbucket.org:slepc/slepc4py.git
        git clone https://bitbucket.org/mapdes/ffc.git
        git clone https://bitbucket.org/mapdes/ufl.git
        git clone https://bitbucket.org/mapdes/fiat.git
        git clone https://bitbucket.org/fenics-project/instant.git
        git clone git@github.com:coneoproject/COFFEE.git
        git clone https://github.com/firedrakeproject/firedrake.git
        git clone git://github.com/OP2/PyOP2.git
    else
        cd $SOURCE_DIR
    fi

    # Install Python packages in prescribed location given by FIREDRAKE_DIR
    if [ ! "$2" = "firedrake" ]
    then
        for PACKAGE in ufl ffc fiat instant COFFEE cython
        do
            echo "Installing $PACKAGE"
            cd $PACKAGE
            git checkout master
            git pull
            python setup.py install --prefix=$FIREDRAKE_DIR
            cd ..
        done

        cd petsc
        git remote add mapdes git@bitbucket.org:mapdes/petsc.git
        git fetch --all
        git checkout $PETSC_BRANCH

        # This patch may not apply for non-Debian systems
        patch -p0 -N -i ../../../petsc-debian-new.diff
        ./configure     --CXX_LINKER_FLAGS=-Wl,--no-as-needed \
                        --download-spai=yes \
                        --download-suitesparse=1 \
                        --useThreads \
                        --with-blacs-include=/usr/include \
                        --with-blacs-lib=[/usr/lib/libblacsCinit-openmpi.so,/usr/lib/libblacs-openmpi.so] \
                        --with-blacs=1 \
                        --with-blas-lib=-lblas \
                        --with-c-support \
                        --with-clanguage=C++ \
                        --with-debugging=0 \
                        --with-fftw-include=/usr/include \
                        --with-fftw-lib=[/usr/lib/x86_64-linux-gnu/libfftw3.so,/usr/lib/x86_64-linux-gnu/libfftw3_mpi.so] \
                        --with-fftw=1 \
                        --with-etags=0 \
                        --with-fortran-interfaces=1 \
                        --download-hdf5=1 \
                        --with-hypre-dir=/usr \
                        --with-hypre=1 \
                        --with-lapack-lib=-llapack \
                        --with-ml=1 \
                        --download-ml=1 \
                        --with-mpi-dir=/usr/lib/openmpi \
                        --with-mumps-include=/usr/include \
                        --with-mumps-lib=[/usr/lib/libdmumps.so,/usr/lib/libzmumps.so,/usr/lib/libsmumps.so,/usr/lib/libcmumps.so,/usr/lib/libmumps_common.so,/usr/lib/libpord.so] \
                        --with-mumps=1 \
                        --with-ptscotch-include=/usr/include/scotch \
                        --with-ptscotch-lib=[/usr/lib/libptesmumps.so,/usr/lib/libptscotch.so,/usr/lib/libptscotcherr.so] \
                        --with-ptscotch=1 \
                        --with-scalapack-include=/usr/include \
                        --with-scalapack-lib=/usr/lib/libscalapack-openmpi.so \
                        --with-scalapack=1 \
                        --with-shared-libraries \
                        --with-spai=1 \
                        --with-spooles-include=/usr/include/spooles \
                        --with-spooles-lib=/usr/lib/libspooles.so \
                        --with-spooles=1 \
                        --with-suitesparse=1 \
                        --with-threadcomm=1 \
                        --download-triangle=1 \
                        --with-vtk-include=/usr/include/vtk-5.8 \
                        --with-vtk=1 \
                        --download-chaco=1 \
                        --with-chaco=1
        make PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH all
        cd ..
        cd slepc
        ./configure --with-arpack
        make SLEPC_DIR=$SLEPC_DIR PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH
        cd ..
        cd petsc4py
        git remote add mapdes git@bitbucket.org:mapdes/petsc4py.git
        git fetch --all
        git checkout $PETSC4PY_BRANCH
        python setup.py build
        python setup.py install --prefix=$FIREDRAKE_DIR
        cd ..
        cd slepc4py
        python setup.py build
        python setup.py install --prefix=$FIREDRAKE_DIR
        cd ..
    fi

    if [ ! "$2" = "no-firedrake" ]
    then
        cd PyOP2
        git checkout $PYOP2_BRANCH
        python setup.py install --prefix=$FIREDRAKE_DIR
        cd ..

        cd firedrake
        git checkout $FIREDRAKE_BRANCH
        python setup.py install --prefix=$FIREDRAKE_DIR
        cd ..
    fi
fi
