\section{Finite Volumes as a Discontinuous Galerkin scheme}\label{app:fvmdg}
Schlumberger discretises their reservoir models using finite volumes.
In this report we use a discontinuous Galerkin finite element formulation
so that Firedrake\citep{rathgeber2015} and the UFL language \citep{alnaes2014} may be
employed.
The piecewise constant DG formulation is equivalent to the finite
volume approach. To see this, consider the following PDE on
$\Omega\subset\mathbb R^n$ with
homogeneous Neumann BCs:
\begin{equation}
  \frac{\partial u}{\partial t} - \Delta u = 0. \label{eq:diffusionpde}
\end{equation}
For the following, let $\{\Omega_i\mid i\in \mathcal I\}$ be a
partition creating a
structured grid of $\Omega$, for some index set $\mathcal I$.
Define $\tau_{ij} = \Omega_i\cap \Omega_j$, and let $\Gamma_{int}$
denote the union of all the interior facets.
$\mathcal N(i)$ is the set of indices $j$ so that $|\tau_{ij}|>0$.
Let $\mathcal V_h$ be a $\mathbb P^0_{DG}$ space with
basis $\{\phi_i = \mathbf 1_{\Omega_i} \mid i\in \mathcal I\}$.

Our $\mathbb P^0_{DG}$ approximation $u=\sum_{i\in\mathcal I}u_i\phi_i$ of
\cref{eq:diffusionpde} satisfies
\begin{align}
  \int_{\Omega} \frac{\partial u}{\partial t}\,v\dx
  + \int_{\Gamma_{int}} \frac{u^+-u^-}{\|h^+-h^-\|}(v^+-v^-)\ds = 0
  && \forall v\in \mathcal V_h.
\end{align}
For a given ordering of $\mathcal I$, the notation $u^+$ and $u^-$
is used denote the limit value of $u$ taken from the two cells that
share a given facet. Here $h(s)$ denotes the center point of the cell
containing the point $s$.

The $\mathbb P^0_{DG}$ formulation comes from the cell-wise weak form of
\cref{eq:diffusionpde}. $u$ must satisfy
\begin{align}
  \int_{\Omega_i} \frac{\partial u}{\partial t}v\dx + \int_{\Omega_i}
  \nabla u\cdot \nabla v\dx
  - \int_{\partial \Omega_i} v \frac{\partial u}{\partial n}\ds = 0
  && \forall i\in \mathcal I, v\in \mathcal V_h. \label{eq:weak_diffusion}
\end{align}
On the interior facets the flux is not defined, so we choose
a flux approximation and set
\begin{align}
  \int_{\partial \Omega_i} v\frac{\partial u}{\partial n}\ds :=
  \sum_{j\in \mathcal N(i)} \int_{\tau_{ij}} v|_i
  \frac{u|_j-u|_i}{\|h|_j-h|_i\|}\ds.
  \label{eq:dg0_diffusion}
\end{align}
For our $\mathbb P^0_{DG}$ space $\mathcal V_h$, $v$ is piecewise constant and
$\nabla v = 0$ in each cell.
Hence, \cref{eq:weak_diffusion} becomes
\begin{align}
  \int_{ \Omega_i} \frac{\partial u}{\partial t}v\dx -  \sum_{j\in \mathcal N(i)}
  \int_{\tau_{ij}} v_i \frac{u_j-u_i}{\|h_j-h_i\|}\ds = 0
  && \forall i\in \mathcal I, v\in \mathcal V_h. \label{eq:weak_diffusion_dg0}
\end{align}
We now sum over $\mathcal I$ in \cref{eq:weak_diffusion_dg0}, and
note that each interior facet is visited twice.
With the ``+'',``-'' style ordering of $\mathcal I$, this yields the $\mathbb P^0_{DG}$
formulation \cref{eq:dg0_diffusion}.

If we instead consider \cref{eq:weak_diffusion_dg0} in its own
right, we get
\begin{align}
  \frac{\partial u_i}{\partial t}|\Omega_i| - \sum_{j\in \mathcal
  N(i)}\frac{u_j-u_i}{\|h_j-h_i\|}|\tau_{ij}| = 0
  && \forall i \in \mathcal I.
\end{align}
This is exactly the finite volume formulation of
\cref{eq:diffusionpde} with the flux approximation
$\frac{\partial u}{\partial n}\big|_{\tau_{ij}}=\frac{u_j-u_i}{h_{ij}}$.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% tex-my-masterfile: "main.tex"
%%% End:
