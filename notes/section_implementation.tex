\section{Implementation}
A significant part of this project was spent
setting up an environment to experiment
with different approaches when solving the discretised equations.
We call the implementation \emph{Oildrake}. It is written in Python and
built on top of \emph{Firedrake} \citep{rathgeber2015} and
\emph{PETSc} \citep{petsc-user-ref,petsc-efficient,dalcin2011}.

PETSc, the
\emph{Portable, Extensible Toolkit for Scientific Computation},
is used to solve the nonlinear system arising at
each timestep. It makes experiments with the solvers easy:
changing between sophisticated solvers may be done with command-line
options. For example running a PETSc program with the arguments
\verb;-ksp_type gmres; \verb;-pc_type ilu; tells the software to solve
a linear system using ILU(0)-preconditioned GMRES. Changing to a direct
LU-solver can be done by passing \verb;-ksp_type preonly; \verb;-pc_type lu; instead.
The options \verb;-snes_type newtonls; and \verb;-snes_type qn;
tell PETSc to use basic Newton and Quasi-Newton nonlinear solvers respectively.

Firedrake is an automated system for solving partial
differential equations using the finite element method.
It is closely related to \emph{the FEniCS project}~\citep{logg2012},
and uses some of the same components for turning
a high-level mathematical finite-element language into efficient
low-level code.
It makes implementing the system of equations easy, as the code
written
strongly resembles \cref{eq:weak}. The high-level mathematical
formulation enables Firedrake to employ exact derivatives of the
equations using automatic differentiation \citep{alnaes2014}.
Firedrake was chosen over FEniCS because we needed support for
hexahedra, which only Firedrake has at the time of writing.

\subsection{Oildrake capabilities}
We wish to experiment with nonlinear solvers, keeping
other factors as close to \emph{ECLIPSE E300} as possible.
Notable capabilities and solver decisions are:
\begin{itemize}
\item Conforming rectangular cuboids in $\mathbb R^3$.
\item FGMRES with the Constrained Pressure Residual (CPR)
  preconditioner~\citep{cao2005}.
\item Fully implicit backward Euler time-discretisation.
\item Adaptive heuristic timestepping based on the predicted
  saturation change.
\item Anisotropic, heterogeneous permeability fields.
\item Heterogeneous porosity fields.
\item Changing the expressions of the properties in \cref{app:properties}
is easy, and the new Jacobian is automatically generated.
\item Single-connected wells modelled as point-source/sinks.
\end{itemize}
The component molar densities must be positive, and to prevent
unconstrained solvers from overshooting, we project any negative
values into the feasible region after each nonlinear step.

Oildrake can only run in serial at the moment. Firedrake and PETSc support
parallelism via MPI automatically,
but this does not seem to work for Oildrake. A debugging session is
necessary to find out why.

The convergence criteria used in Oildrake is different from
Schlumberger's reservoir simulators. We terminate based on the
$\ell_2$-norms of the residuals, using the following default values:
\begin{itemize}
\item \emph{Nonlinear solver}:  Relative tolerance = $10^{-8}$, absolute
  tolerance = $10^{-3}$, maximum iterations = 20.
\item \emph{Linear solver}: Relative tolerance = $10^{-5}$, absolute
  tolerance = $10^{-10}$, maximum iterations = 20.
\end{itemize}
For E300, the convergence criteria is a custom max-norm on the
residual of the change in saturations. In practice this terminates the
solver much earlier than Oildrake, i.e. Oildrake solves the equations to a
tighter tolerance.

\subsubsection{Validation against ECLIPSE}
To ensure that the simulator works, we can compare it with ECLIPSE
E300 on test models.
\Cref{fig:comparison_spe10small} shows a selection of cells in a model
with permeability field derived from the SPE10 comparative solution
project~\citep{christie2001}. Both simulators predict the same flow patterns.

The initial pressure is set to $250$ bar in the
whole reservoir. We start the system in a highly unstable configuration,
with three single phase layers water, oil and gas on top of each other.
Due to mass density differences, the fluids flow due to
gravity. Horizontal flow happens as a result of the heterogeneous
permeability field shown in \cref{fig:permeability_spe10small}.

\begin{figure}[htdp]
  \centering
  \includegraphics[width=\textwidth]{./img/kx-spe10small.png}
  \caption{Permeability field for the SPE10Small model. It is highly
    varying, causing flow rates to be very different between
    cells.}\label{fig:permeability_spe10small}
\end{figure}

\begin{figure}[htdp]
  \centering
  \includegraphics[width=\textwidth]{./img/solution_comparison.eps}
  \caption{Gas inversion comparison between Oildrake and ECLIPSE, for select cells
    in the SPE10Small model. The black striped curves are the ECLIPSE
    solutions, which fit very well with our implementation. Note that
    the legend cells are given in ECLIPSE $ijk$-ordering, where the
    $k$'s increases as we go down in the
    reservoir.}\label{fig:comparison_spe10small}
\end{figure}


\subsection{Discrete system and linearisation}
Oildrake solves the semi-discrete system in \cref{eq:weak}
using a backward Euler discretisation.
For a reservoir with $N$ cells, this results in a nonlinear system
$F_n:\mathbb R^{4N}\to\mathbb R^{4N}$ to be solved at each timestep.

The standard method to solve this system is Newton's method.
Most of the taken by the nonlinear solver is spent
finding the step direction given by the linearised system.
\begin{figure}[htdp]
  \centering
  \includegraphics[width=0.7\textwidth]{./img/spy_jacobian.eps}
  \caption{Sparsity pattern of the Jacobian from SPE10Small at time
    0.1. The first 1000 rows/columns represent $m_o$, then comes
    $m_g$, $m_w$ and $p$.
  }\label{fig:spy_jacobian}
\end{figure}

\Cref{fig:spy_jacobian} shows the sparsity pattern of the
Jacobian arising from backward Euler.
The different character of the pressure part of the system is the
motivation behind the Constrained
Pressure Residual (CPR)
preconditioner~\citep{cao2005,ecltechnical2015}.

In reservoir simulation, FGMRES right preconditioned with CPR
proves to be very effective.
It is a composite multiplicative preconditioner. The
first stage preconditioner is a Schur type approximation to the
``pressure''-part of our
equations \cref{eq:volume_constraint}. It applies one V-cycle of algebraic multigrid (AMG) to the
approximate Schur complement, updating the pressure variables.
The second stage uses a ILU(0) preconditioner on the whole system.
Solving for the Newton step direction using FGMRES+CPR takes
5-10 iterations on average in our tests. All parts of the linear solver
scale well in size and cores, apart from the AMG setup
\citep{baker2012}. Reducing the AMG setup costs is important for practical problems,
since they are increasingly solved on many-core systems.

For all our tests, we have used the Quasi-IMPES Schur
approximation. In the preconditioning literature, this makes the inner
CPR similar to a SIMPLE-type preconditioner \citep{ur2010}.

% Let superscript $n$
% denote function values at time $t^n$, and
% set $\Delta t^n=t^n-t^{n-1}$.
% In a reservoir with $N$ cells, the solution is determined
% at each step $n$ by finding the
% root of $F_n:\mathbb R^{4N}\to\mathbb R^{4N}$, resulting from
% \begin{subequations}\label{eq:backward_euler}
%   \begin{align}
%     \Delta t^n\left( \int_{\Gamma_{int}}\widetilde{K}\widetilde{\lambda_\alpha}^n\right.
%     \left.\left( \widetilde{\nabla p}^n +
%     \widetilde{\rho_\alpha}^n\mathbf g \right)(v^+-v^-)\ds
%     -\int_\Omega \sigma_\alpha^n v\dx \right) + & &&\forall v\in
%                                                      \mathcal V_\alpha, \nonumber \\
%      \int_\Omega \left( \phi^n m_\alpha^n -  \phi^{n-1}
%     m_\alpha^{n-1}\right) v\dx
%     &= 0
%     && \alpha=g,o,w. \\
%     \int_\Omega \left( 1 - \sum_\alpha {(S_\alpha)}^n \right) v\dx &=0  &&
%     \forall v \in \mathcal V_p. \label{eqn:volume_constraint_euler}
%   \end{align}
% \end{subequations}
% The go-to method for solving such systems is the Newton method, listed
% in \cref{alg:newton}. With a good initial guess, Newton's method
% converges in few iterations. In time-dependent PDEs, the solution at
% the previous timestep is used as an initial guess solving the
% nonlinear system. As long as the timestep is not too long, this
% provides a good initial guess for the Newton method.
% \begin{algorithm}
%   \caption{Newton's method for finding roots of
%     $F:\mathbb R^k\to\mathbb R^k$ with
%   with Jacobian $J(x)=\frac{\partial F(x)}{\partial x}$.}\label{alg:newton}
%   \begin{algorithmic}
%     \While {Solution not converged}
%     \State $d \gets J(x^i)^{-1}F(x^i)$ \Comment{Approximate inversion,
%     eg using a Krylov method.}
%     \State $x^{i+1}\gets x^i - d$
%     \EndWhile
%   \end{algorithmic}
% \end{algorithm}





%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% tex-my-masterfile: "main.tex"
%%% End:
