\section{Governing equations}
The equations solved by our reservoir model are from
Schlumberger's ECLIPSE reservoir simulator~\citep{eclreference2015,ecltechnical2015}.
We consider the simplest case, a black oil model where no components
can change phase.
A derivation of the equations can be found in \citep{christensen2012}.
ECLIPSE solves the system using finite volume methods, whilst our
formulation is written in the finite element framework. Using
zero-order discontinuous Galerkin elements and the same numerical
flux approximation, they result in the same numerical system.
The equivalence of the two methods is described for the diffusion
equation in \cref{app:fvmdg}.


\subsection{Assumptions}
We model the flow in the reservoir using three immiscible components:
oil, gas and water. Each component corresponds to one phase in our
fluid model; liquid, vapour and water respectively. In the oil
industry this is sometimes referred to as a dead oil, dry gas-system.
Capillary pressures are also omitted.

Let the reservoir be denoted as $\Omega\subset \mathbb R^3$
throughout.
The primary variables of the system are component molar densities
$m_o, m_g, m_w$ of oil, gas and water, and pressure $p$.
Their dimensions are $\mathrm{\frac{mol}{L^3}}$ and
$\mathrm{\frac{M}{LT^2}}$. Together with the properties in
\cref{tbl:properties_pde} we can fully describe the system.
Constitutive relationships define the underlying equations.
An example of such relations are given in \cref{app:properties},
resulting in the nonlinear systems used for this report.

\begin{table}[htdp]
  \centering
  \begingroup
  \renewcommand*{\arraystretch}{1.2}
  \begin{tabular}{lcc}
    \textbf{Property} & \textbf{Symbol} & \textbf{Dimension}\\
    \toprule
    Porosity & $\phi$ & -\\
    Phase molar density & $b_\alpha$ & $\mathrm{\frac{mol}{L^3}}$ \\
    Darcy velocity & $v_\alpha$ & $\mathrm{\frac{L}{T}}$ \\
    External source/sink & $\sigma_\alpha$  & $\mathrm{\frac{mol}{L^3T}}$ \\
    Absolute permeability tensor & $K$ & $\mathrm{L^2}$ \\
    Relative permeability & $k_{r\alpha}$ & -\\
    Dynamic viscosity & $\mu_\alpha$ & $\mathrm{\frac{M}{LT}}$\\
    Mass density & $\rho_\alpha$ & $\mathrm{\frac{M}{L^3}}$\\
    Gravity & $g$ &$\mathrm{\frac{L}{T^2}}$ \\
    Saturation & $S_\alpha$ & -
  \end{tabular}
  \endgroup
  \caption{Overview of the reservoir properties of our model,
    including their dimension. They relate to our decision variables
    through the constitutive equations given in \cref{app:properties}.
    Wells are modelled using $\sigma_\alpha$.
    }\label{tbl:properties_pde}
\end{table}

The absolute permeability $K$ is a diagonal matrix where
the entries represent rock permeability along the three axes respectively.

\subsection{Strong and weak formulation}
The porous media flow model we use is derived from mass balance
principles and a Darcy approximation for the flow.
We work in a standard right-hand Cartesian coordinate frame,
and set $\mathbf g=(0,0,-g)^T$. Note that ECLIPSE uses a left-hand
frame, where the positive $z$-direction is downward.
For a given initial configuration in $\Omega$, find $p,m_\alpha$,
$\alpha=g,o,w$ such that
\begin{subequations}
  \begin{align}
    \frac{\partial \phi m_\alpha}{\partial t} +
    \nabla\cdot (b_\alpha v_\alpha) &= \sigma_\alpha & \text{Mass balance}\\
    v_\alpha &= -K\frac{k_{r\alpha}}{\mu_\alpha}\left( \nabla p -
               \rho_\alpha \mathbf g \right) & \text{Darcy flow} \\
    \sum_\alpha S_\alpha &= 1  & \text{Volume constraint}\label{eq:volume_constraint}\\
    \frac{\partial b_\alpha v_\alpha} {\partial n} &= 0
                                                     &\text{No normal flow through } \partial \Omega.
  \end{align}
\end{subequations}

One could use the volume constraint to eliminate one of the
$m_\alpha$'s. Reservoir simulators approach this
in different ways: \emph{ECLIPSE E300} leaves it explicitly, whilst \emph{INTERSECT}
eliminates one primary variable \citep{ecltechnical2015,ixtechnical2015}.
An argument for keeping all variables is that it gives the nonlinear
solver a larger navigation space. One can take paths towards the root
that leave the manifold defined by the constraint.
As our simulator wants to replicate its
behaviour, we have left the constraint in explicitly.

To solve the system, a finite volume discretisation is used.
We formulate it in an equivalent weak sense, using piecewise constant
discontinuous Galerkin elements ($\mathbb P^0_{DG}$).
Background reading on the discontinuous Galerkin methods may be found
in \citep{riviere2008}, including higher order examples for porous
media flow.

We express the semi-discrete set of equations here. The resulting
system of ODEs is solved using a fully implicit backward Euler scheme.
For a given structured grid $\Omega_h$ of $\Omega$, define
$\Gamma_{int}$ to be the set of interior facets.
Define the mobility for a fluid to be
$\lambda_\alpha = \frac{b_\alpha k_{r\alpha}}{\mu_\alpha}$.
Let $\mathcal V_h = (\mathcal V_g, \mathcal V_o, \mathcal V_w,
\mathcal V_p)$ be the $\mathbb P^0_{DG}$ space on $\Omega_h$.
Find $(m_g,m_o,m_w,p)\in \mathcal V_h$ such that
\begin{subequations}\label{eq:weak}
  \begin{align}
    \int_\Omega \frac{\partial\phi m_\alpha}{\partial t} v\dx +
    \int_{\Gamma_{int}}\widetilde{K}\widetilde{\lambda_\alpha}
    \left( \widetilde{\nabla p} +
    \widetilde{\rho_\alpha}\mathbf g \right)(v^+-v^-)\ds
    &= \int_\Omega \sigma_\alpha v\dx
    && \forall v\in \mathcal V_\alpha, \alpha=g,o,w  \\
    \int_\Omega\left( 1- \sum_\alpha S_\alpha \right) v\dx &= 0 && \forall v \in \mathcal V_p.
  \end{align}
\end{subequations}
The terms $v^+,v^-$ refer to the limit value on a facet, taken from
adjacent cells $\Omega^+, \Omega^-$ respectively.
Let $\hat n$ denote the positive unit normal for the facets.
We approximate the flux across facets with an
upwind Goudunov method, by setting
\todo[inline]{The definition of $\widetilde{K}$ is wrong.
It should be defined as the element-wise harmonic mean.}
\begin{subequations}
\begin{align}
  \widetilde{K} &= \frac{1}{2}\left( \frac{K^+ + K^-}{K^+\circ K^-} \right)\hat n &&
  \text{where }\circ \text{is the Hadamard product,} \\
  \widetilde{\lambda_\alpha} &= \left\{
  \begin{array}{ll}
    \lambda_\alpha^- & \Phi_\alpha < 0 \\
    \lambda_\alpha^+ & \Phi_\alpha \geq 0
  \end{array}
  \right.
  && \Phi_\alpha =\widetilde{\nabla p} +
  \widetilde{\rho_\alpha}\mathbf g, \\
  \widetilde{\nabla p} &= \frac{p^+-p^-}{\|h^+-h^-\|}
  && h \text{ denotes cell centres,} \\
  \widetilde{\rho_\alpha} &= \frac{{S_\alpha}^+\rho_\alpha^+ + {S_\alpha}^-\rho_\alpha^-}
  {{S_\alpha}^+{S_\alpha}^-}
  && \text{a saturation weighed average}.
\end{align}
\end{subequations}
The permeability and mass density approximations can cause division by
zero problems. For mass density, this is taken care of by taking the
arithmetic average in the absence of a phase in two adjacent cells.
For permeability, the face is considered impermeable, and $\widetilde{K}:=0$.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% tex-my-masterfile: "main.tex"
%%% End:
