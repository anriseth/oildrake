\section{Solver experiments}
The intention of Oildrake is to easily test new solver techniques
on reservoir models. With PETSc as a solver backend, this can be done
setting options before running the simulation.
The efficiency of solvers is problem-specific. There is a given amount
of work needed to be done, and different approaches basically move the work
to different subcomponents.
For example, lagging the preconditioner may decrease the number of
preconditioner setups, at the expense of running more Krylov iterations.
In this section, we show the result of two approaches to modify the
workload balance:
preconditioner lagging and NGMRES with Newton as a preconditioner.
We will show what options to pass to PETSc in order
to use each approach.
In addition, any linear solver settings must be passed to the Newton
component.

Lagging the preconditioner is an example of a tactic of
reducing accuracy of single iterations to see if
what one can get away with.
NGMRES on the other hand is used a nonlinear Krylov method used to accelerate each Newton
step, making each step more efficient.

The tests on Oildrake show that NGMRES with nonlinear preconditioning
performs better than basic Newton, whilst naive preconditioner lagging does
worse for the test cases considered.

As well as the two methods presented here, a Quasi-Newton (QN) solver was also tested as part of this project.
The idea behind it is to trade more nonlinear iterations for solving
the same matrix system. QN assembles the Jacobian at a given
step, and then does rank-one updates to approximate the linear system
in consecutive steps.
For problems where basic Newton is stagnant over several iterations, QN can
replace these expensive steps with much cheaper ones.
Using nonlinear solver composition, we can use QN to take us
cheaply closer to the correct solution, before falling back to Newton
with its great convergence properties.
In the
test cases considered, basic Newton is already in the quadratic
regime, so this trade did not pay off and we do not present any of
these results.


\subsection{Application specific timings}
In interpreting the results from Oildrake, we stress that
the relative runtime of different subcomponents of the solvers
can't be mapped to performance in ECLIPSE or INTERSECT.
The most important results to take from these runs are the number of calls
to different subcomponents of the solvers.
These numbers can give an indication of how the new approaches will do in the
industry simulators.

The two main differences to ECLIPSE and INTERSECT are
\begin{enumerate}
\item \emph{Parallelism}: Industry simulators are run in parallel, but
  these tests have been done in serial.
  Some components scale well in parallel, others don't.
  For large problems, we want to shift work from nonscalable to
  scalable subproblems.
\item \emph{Evaluation and assembly}:
  Oildrake's Jacobian assembly and function evaluation has not been
  optimised to the same degree as industry simulators.
\end{enumerate}
The run time per operation varies between runs in Oildrake. Caching
and other activity on the computer are two causes for this.


\subsection{NGMRES preconditioned with basic Newton}
Petsc options: \verb;-snes_type ngmres -snes_ngmres_restart_fm_rise;
\newline
\verb;-snes_npc_side right -npc_snes_type newtonls;

Basic Newton forgets its iteration history, and will only use
information at the current step to decide where to move forward.
Nonlinear GMRES will also use the insight of previous iterations
to accelerate convergence.
It is suggested in \citep{sterck2013} that NGMRES preconditioned with
an application specific nonlinear solver can improve convergence.

\Cref{alg:ngmres} describes the NGMRES algorithm as it is implemented
in PETSc. We won't go in to the sufficiency requirements in the
iteration update, but refer to the PETSc documentation.

\begin{algorithm}
  \caption{Right-preconditioned NGMRES}\label{alg:ngmres}
  { \setstretch{1.5}
  \begin{algorithmic}
    \Procedure{NGMRES}{$F, x_i, \dots, x_{i-m+1}$}
    \State $x_i^M = M(x_{i-1})$ \Comment $M$ is the preconditioner
    \State $F_i^M = F(x_i^M)$
    \State minimize $\left\|F\left( \left(
          1-\sum_{k=i-m}^{i-1}\alpha_i\right)x_i^M +
        \sum_{k=i-m}^{i-1}\alpha_k x_k \right) \right\|_2$
    over $\{\alpha_{i-m},\dots\alpha_{i-1}\}$
    \State $x_i^A =\left(
      1-\sum_{k=i-m}^{i-1}\alpha_i\right)x_i^M +
    \sum_{k=i-m}^{i-1}\alpha_k x_k$
    \State $x_{i+1}=x_i^A$ or $x_i^M$ if $x_i^A$ is insufficient.
    \EndProcedure
  \end{algorithmic}
}
\end{algorithm}

Preliminary tests with NGMRES showed stagnation for certain timesteps.
The cause was that the Newton preconditioner wanted to step in a
direction that significantly increased the residual, while NGMRES
hindered this step. A Newton step increasing the residual may be an
attempt to move out of a stagnation area, and into an area where
convergence can
go more rapidly.

This was dealt with by resetting the Krylov subspace
after such an attempt, which improved the performance of the algorithm
for our test problems. One can tell PETSc to do this
by passing the option \verb;-snes_ngmres_restart_fm_rise;.
The feature was contributed as part of this mini-project, and should be
available in releases after version 3.6.0.

\Cref{tbl:ngmres_runs,tbl:ngmres_timings} show that accelerating the
Newton steps with NGMRES indeed reduces the number of nonlinear solves
needed per timestep. The main cost is extra function evaluations, but
overall the time savings are greater in our tests.
\begin{table}[htdp]
  \centering
  \begin{tabular}{lrrrrrr}
    &\multicolumn{2}{c}{\textbf{Nonlinear solves}}
    &\multicolumn{2}{c}{\textbf{Linear solves}}
    &\multicolumn{2}{c}{\textbf{Residual evaluations}} \\
    Test&Newton&NGMRES&Newton&NGMRES&Newton&NGMRES\\
    \toprule
    SPE10&854&769&6030&5278&1037&1721\\
    Hetero&1113&991&6333&5534&1349&2218\\
    Hetero25k&1719&1531&10000&8642&2095&3438\\
    Hetero100k&2740&2479&15906&14003&3317&5535
  \end{tabular}
  \caption{Comparison of number of operations using basic Newton and NGMRES.
  NGMRES reduces the number of nonlinear iterations by about 10\%,
  at the expense of increasing the number of residual evaluations by
  60\%-70\%.
  There is one preconditioner assembly per nonlinear solve.}\label{tbl:ngmres_runs}
\end{table}

\begin{table}[htdp]
  \centering
  \begin{tabular}{lrrrrr}
    Test&Total time&Linear&PC Setup& Residual&Jacobian\\
    \toprule
    SPE10 Basic&73.2&14.1&6.5&14.5&41.6\\
    SPE10 NGMRES&75.6&11.5&5.5&21.6&36.2\\
    \midrule
    Hetero Basic&457.9&180.6&108.6&57.8&210.4\\
    Hetero Hetero25k&465.9&166.1&97.6&93.4&187.7\\
    \midrule
    Hetero25k Basic&1282.4&552.4&331.7&145.8&558.8\\
    Hetero25k NGMRES&1253.4&490.5&297.2&235.6&496.4\\
    \midrule
    Hetero100k Basic&8832.5&4179.3&2524.1&866.9&3610.8 \\
    Hetero100k NGMRES&8554.7&3643.0&2218.9&1410.6&3312.0
  \end{tabular}
  \caption{Run time comparison of different parts of the nonlinear solvers
    in seconds. NGMRES reduces runtime for large problems.
    As the problems get larger, the savings from reducing
    the number of nonlinear iterations needed are greater than the
    cost of extra
    function evaluations.}\label{tbl:ngmres_timings}
\end{table}
\clearpage
\subsection{Preconditioner lagging}
PETSc option:
\verb;-snes_preconditioner_lagging <number>;

The preconditioner setup is costly and does not scale as well as other
solver components. This motivates the attempt to set up the
preconditioner, then reuse it for later linear systems.

In our tests, lagging the preconditioner significantly reduces the
number of PC set ups. \Cref{tbl:lag_runs,tbl:lag_timings} show the
results of setting up the preconditioner every second and third
nonlinear iteration. The extra number of linear solves used is less than
twice the reduction in number of PC set ups.
Running in serial with Oildrake, the extra cost of linear and
nonlinear steps was larger than the savings in PC set ups.

The lagged preconditioner usually performed better in later Newton
iterations.
We hypothesise that the step lengths are shorter at these iterations,
so the lagged preconditioner approximates the current step better.
Making preconditioner lagging decisions based on the length of
the previous nonlinear step may preserve sufficient accuracy.
We did not have time to test this in Oildrake.

The first lagged preconditioner can often take more than 100
iterations. In our tests, we capped them at 20 per Newton step.
As a result, the number of nonlinear solves needed per step has increased,
as can be seen in \cref{tbl:lag_runs}.

\begin{table}[htdp]
  \centering
  \begin{tabular}{lrrrrr}
    Test&
        \shortstack[r]{Nonlinear \\ solves}
        &\shortstack[r]{Linear \\ solves}
        &PC Setups&Residuals\\
    \toprule
    SPE10 Basic&854&6030&854&1037\\
    SPE10 Lag 2&863&7604&479&1046\\
    SPE10 Lag 3&868&9158&354&1051\\
    \midrule
    Hetero Basic&1113&6333&1113&1349\\
    Hetero Lag 2&1159&11066&660&1395\\
    Hetero Lag 3&1282&15388&508&1518\\
    \midrule
    Hetero25k Basic&1719&10000&1719&2095\\
    Hetero25k Lag 2&1804&16895&1028&2180\\
    Hetero25k Lag 3&2016&23666&811&2392\\
    \midrule
    Hetero100k Basic&2740&15906&2740&3317\\
    Hetero100k Lag 2&2910&28571&1626&3488\\
    Hetero100k Lag 3&3367&41910&1364&3945
  \end{tabular}
  \caption{
    Comparison of number of operations using basic Newton and
    preconditioner lagging.
    Lagging the preconditioner once decreases the PC setups
    by about $40\%$. In the SPE10 case, it increases the number of
    linear solves by 26\%. In the Hetero-tests, the linear solves required increased
    by 70\%-80\%.}\label{tbl:lag_runs}
\end{table}

\begin{table}[htdp]
  \centering
  \begin{tabular}{lrrrrr}
    Test&Total time&Linear&PC Setup&Residual&Jacobian\\
    \toprule
    SPE10 Basic&73.7&14.6&6.5&14.4&41.9\\
    SPE10 Lag 2&73.9&13.3&3.8&14.7&42.7\\
    SPE10 Lag 3&73.2&13.6&2.8&14.6&42.3\\
    \midrule
    Hetero Basic&476.5&188.2&113.0&58.0&220.5\\
    Hetero Lag 2&492.4&197.2&66.3&60.1&224.9\\
    Hetero Lag 3&583.5&246.7&53.4&66.6&258.4\\
    \midrule
    Hetero25k Basic&1385.6&589.4&350.5&147.2&617.3\\
    Hetero25k Lag 2&1454.3&620.5&210.7&153.0&653.7\\
    Hetero25k Lag 3&1585.6&707.5&159.8&168.6&677.2\\
    \midrule
    Hetero100k Basic &8894.0&4109.0&2488.7&834.4&3774.2\\
    Hetero100k Lag 2&9740.6&4608.7&1504.0&886.6&4066.4\\
    Hetero100k Lag 3&11509.1&5621.6&1236.8&999.1&4687.5
  \end{tabular}
  \caption{Run time comparison of different parts of the nonlinear solvers
    in seconds. The relative PC setup costs on small problems in  serial are never large
    enough to justify the
    additional linear iterations needed.}\label{tbl:lag_timings}
\end{table}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% tex-my-masterfile: "main.tex"
%%% End:
