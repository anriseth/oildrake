\section{Properties and input values}\label{app:properties}
The input values used for the experiments in this report are given in
the same units as ECLIPSE's ``metric'' setting. These cause the units of the accumulation and
flux terms to differ, so the flux term must be multiplied by a
conversion factor.
The factor is called a Darcy constant, and is defined as
$C_{Darcy}=8.52702\times 10^{-3} \mathrm{cPm^2/day/bar}$
\citep{eclreference2015}. We note that ``day'' is not an SI unit.

\begin{table}[htdp]
  \centering
  \begin{subtable}[t]{.5\textwidth}
    \centering
    \begin{tabular}{clcc}
      \textbf{Symbol} & \textbf{Value} & $\mathbf{\times 10^x}$& \textbf{Units}\\
      \toprule
      $\rho_{sc}^o$ & 8.0 & 2 & $\mathrm{kg/m^3}$ \\
      $\rho_{sc}^g$ & 9.907&-1 & $\mathrm{kg/m^3}$ \\
      $\rho_{sc}^w$ & 1.022 & 3 & $\mathrm{kg/m^3}$ \\
      $M^o$ & 1.20 &2 & $\mathrm{kg/kmol}$ \\
      $M^g$ & 2.5 &1 & $\mathrm{kg/kmol}$ \\
      $M^w$ & 1.8025 &1 & $\mathrm{kg/kmol}$ \\
      $g$ & 9.80665 & -5 & $\mathrm{m^2kg/bar}$
    \end{tabular}
    \caption{Physical constants.}
  \end{subtable}%
  \begin{subtable}[t]{.5\textwidth}
    \centering
    \begin{tabular}{clcc}
      \textbf{Symbol} & \textbf{Value} & $\mathbf{\times 10^x}$& \textbf{Units}\\
      \toprule
      $p_{ref}$ & 2.5 & 2 & bar \\
      $B_{pref}^w$ & 1.03 & 0 & - \\
      $\mu_{ref}^w$ & 3.0 & -1 & cP \\
      $C_w$ & 0.0 & 0 & 1/bar \\
      $C$ & 4.1 & -5 & 1/bar \\
      $C_{rock}$ & 5.3 & -5 & 1/bar
    \end{tabular}
    \caption{Reservoir specific input values.}
  \end{subtable}
  \caption{Input values used when calculating the value of the
    properties listed in this section. Water is considered
    incompressible.}\label{tbl:input_values}
\end{table}

\subsection{Properties}
The expressions used for the properties of the fluids from
\cref{tbl:properties_pde} are listed
here. Our experiments have been run with the constant values in
\cref{tbl:input_values}.
For further explanation of the physical meaning of the
following properties, see \citep{christensen2012}.
% \Cref{tbl:properties} lists the properties and what values they depend
% on.

% \begin{table}[htdp]
%   \centering
%   \begin{tabular}{lcl}
%     \textbf{Property} & \textbf{Value} & \textbf{Dependants}\\
%     \toprule
%     Formation volume factor & $B_\alpha$ & $p$ \\
%     Phase molar density & $b_\alpha$ & $p$ \\
%     Saturation & $S_\alpha$ & $m_\alpha, p$ \\
%     Viscosity & $\mu_\alpha$ & $p$ \\
%     Mass density & $\rho_\alpha$ & $p$ \quad $*(m_\alpha, p)$ \\
%     Relative permeability & $k_{r\alpha}$ & $m_\alpha, p$ \\
%     Porosity & $\phi$ & $p$
%   \end{tabular}
%   \caption{Overview of properties described in this section and their
%     dependency on pressure $p$ and molar densities $m_\alpha$.
%     Mass density depends on both variables when approximated on
%     facets.
%   }\label{tbl:properties}
% \end{table}

% Equations~\eqref{eqn:properties_start} to~\eqref{eqn:properties_end}
% contain all the
% information needed to calculate the properties of the reservoir fluids.
$B_\alpha$ is the \emph{formation volume factor}, and is used to
calculate phase molar density and mass density.

\begin{align}
  B_\alpha &\text{ is given by interpolation using
  \cref{tbl:properties_interpolation}} && \alpha=o,g
  \label{eqn:properties_start}\\
  B^w &= \frac{B_{pref}^w}{1+X+\frac{X^2}{2}} &&
  X=C\cdot(p-p_{ref}) \\
  b_\alpha &= \frac{b_{sc}^\alpha}{B_\alpha} && b_{sc}^\alpha =
  \frac{\rho_{sc}^\alpha}{M_\alpha}   \\
  S_\alpha &= \frac{m_\alpha}{b_\alpha} \\
  \mu_\alpha &\text{ is given by interpolation using
  \cref{tbl:properties_interpolation}} && \alpha=o,g\\
  \mu^w &= \frac{\mu_{pref}^w}{1+X_v+\frac{X_v^2}{2}} &&
  X_v=C_v\cdot(p-p_{ref}) \\
  \rho_\alpha &= \frac{\rho_{sc}^\alpha}{B_\alpha} && \text{in cells}
  \\
  \rho_{\alpha, ij} &= \frac{S_{\alpha,i}\rho_{\alpha,i} +
  S_{\alpha,j}\rho_{\alpha,j}}
  {S_{\alpha,i} + S_{\alpha,j}} && \text{on facets} \\
  k_{r\alpha} &= S_\alpha \\
  \phi &= \varphi\cdot\left(1+X_r+\frac{X_r^2}{2}\right) &&
  X_r=C_{rock}\cdot(p-p_{ref}) \label{eqn:properties_end}
\end{align}
The mass densities on facets are calculated using a saturation weighted
average.
One needs to be careful
if the saturation in both adjacent cells is zero.
In this case, take $\rho_{\alpha, ij} =
\frac{\rho_{\alpha,i}+\rho_{\alpha,j}}{2}$.

Setting relative permeabilities equal to the saturations is an
oversimplification. Better approximations use a quadratic dependence,
possibly with a ``critical saturation'' where $k_{r\alpha}$ is
zero for saturations smaller than this value.


\begin{table}[htdp]
  \begin{subtable}{.5\textwidth}
    \centering
    \begin{tabular}{ccc}
      $p$ & $B^o(p)$ & $\mu^o(p)$ \\
      \toprule
      50 & 1.18 & 0.8 \\
      600 & 1.08 & 1.6
    \end{tabular}
  \end{subtable}%
  \begin{subtable}{.5\textwidth}
    \centering
    \begin{tabular}{ccc}
      $p$ & $B^g(p)$ & $\mu^g(p)$ \\
      \toprule
      50 & $2.05\times 10^{-2}$ & $1.4\times 10^{-2}$ \\
      600 & $3.9\times 10^{-3}$ & $2.5\times 10^{-2}$
    \end{tabular}
  \end{subtable}%
  \caption{Oil and gas interpolation tables
  used to calculate formation volume factors and viscosities.}\label{tbl:properties_interpolation}
\end{table}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% tex-my-masterfile: "main.tex"
%%% End:
