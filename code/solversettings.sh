# This document contains all the solver commands to pass to PETSc

log_gmres_ilu=gmres_ilu.log
log_cpr_1amg=cpr_composite_1amg.log
log_cpr_gmres_ilu=cpr_composite_gmres_ilu.log

file_gmres_ilu=gmres_ilu.dump
file_cpr_1amg=cpr_composite_1amg.dump
file_cpr_gmres_ilu=cpr_composite_gmres_ilu.dump

FGMRES_MAXIT=20
FGMRES_QUASI_MAXIT=20
FGMRES_ATOL=1.0e-10
FGMRES_RTOL=1.0e-5

FGMRES="-od_ksp_type fgmres -od_ksp_max_it $FGMRES_MAXIT -od_ksp_atol $FGMRES_ATOL -od_ksp_rtol $FGMRES_RTOL"
COMPOSITE="$FGMRES -od_pc_type composite -od_pc_composite_type multiplicative -od_pc_composite_pcs python,ilu"
FULL="$FGMRES -od_pc_type python"
SCHUR_1AMG="-od_cpr_stage1_pc_type hypre -od_cpr_stage1_pc_hypre_type boomeramg"

Niter=2
SCHUR_GMRES_ILU="-od_cpr_stage1_ksp_type gmres -od_cpr_stage1_pc_type ilu -od_cpr_inner_ksp_max_it $Niter"

settings_gmres_ilu="-od_ksp_type gmres -od_ksp_max_it 1000 -od_pc_type ilu"
settings_cpr_1amg="$COMPOSITE $SCHUR_1AMG"
settings_cpr_gmres_ilu="$COMPOSITE $SCHUR_GMRES_ILU"

# Set up an inner Quasi-Newton to handle the easier cases
# Revert to Newton if it doesn't work

SNES_MAX_IT=1
SNES_0_MAX_IT=10
SNES_1_MAX_IT=15
SNES_MAX_FAIL=100
SNES_ATOL=1.0e-3
SNES_RTOL=1.0e-10
QN_RESTART=$SNES_0_MAX_IT

COMPOSITESNES="-od_snes_type composite -od_snes_composite_type multiplicative -od_snes_composite_sneses qn,newtonls -od_snes_max_fail $SNES_MAX_FAIL"
QNBROYDEN="-od_snes_qn_restart_type periodic -od_snes_qn_scale_type jacobian -od_snes_qn_type broyden -od_snes_qn_m $QN_RESTART"
QNLS=bt

# TODO: SNESCompositeAddSNES_Composite seem to pass down tolerances, so it shouldn't be needed?

SUB0SNES=${QNBROYDEN//od_/od_sub_0_}
SUB0SNES="$SUB0SNES -od_sub_0_snes_max_it $SNES_0_MAX_IT -od_sub_0_snes_max_linear_solve_fail $SNES_0_MAX_IT -od_sub_0_snes_linesearch_type $QNLS -od_sub_0_snes_atol $SNES_ATOL -od_sub_0_snes_rtol $SNES_RTOL"
SUB1SNES="-od_sub_1_snes_max_it $SNES_1_MAX_IT -od_sub_1_snes_max_linear_solve_fail $SNES_1_MAX_IT -od_sub_1_snes_linesearch_type basic -od_sub_1_snes_atol $SNES_ATOL -od_sub_1_snes_rtol $SNES_RTOL"


SUB0KSP=${COMPOSITE//od_/od_sub_0_}
SUB1KSP=${COMPOSITE//od_/od_sub_1_}
# Make the inner Quasi-Newton cheaper by setting a smaller ksp_max_it
SUB1KSP=${SUB1KSP/"ksp_max_it $FGMRES_MAXIT"/"ksp_max_it $FGMRES_QUASI_MAXIT"}

SUB0="$SUB0SNES $SUB0KSP"
SUB1="$SUB1SNES $SUB1KSP"

settings_qn_composite="$COMPOSITESNES $SUB0 $SUB1"


#################
## NGMRES right preconditioned with NEWTONLS
#################
NPC="-od_snes_npc_side right -od_npc_snes_type newtonls -od_npc_snes_linesearch_type basic"
NPCKSP=${COMPOSITE//od_/od_npc_}
NPCKSP="$NPCKSP $SCHUR_1AMG"

#NPCKSP="-od_npc_ksp_type preonly -od_npc_pc_type lu"

NPCMONITOR="-od_npc_ksp_converged_reason -od_npc_snes_monitor"
NGMRES="-od_snes_type ngmres -od_snes_ngmres_restart_fm_rise"


settings_ngmres_npc="$NGMRES $NPC $NPCKSP"

sub1="-od_sub_0_pc_fieldsplit_0_fields 0,1,2 -od_sub_0_pc_fieldsplit_1_fields 3 \
-od_sub_0_pc_fieldsplit_type schur -od_sub_0_pc_fieldsplit_schur_fact_type diag \
-od_sub_0_pc_fieldsplit_schur_precondition selfp \
-od_sub_0_fieldsplit_0_ksp_type preonly -od_sub_0_fieldsplit_0_pc_type none \
-od_sub_0_fieldsplit_1_ksp_type preonly \
-od_sub_0_fieldsplit_1_pc_type hypre  -od_sub_0_fieldsplit_1_pc_hypre_type boomeramg"
#-od_sub_0_fieldsplit_1_pc_type lu"
cproptions="-od_pc_composite_pcs fieldsplit,ilu $sub1"
