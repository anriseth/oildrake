import numpy as np
import sys

def ecl2npy(fin, fout, dims, minval=0.0, maxval=sys.maxsize):
    """
    ECL files seem to be given with four columns
    (1,1) corresponds to (1,1,1) in ECLIPSE. 
    (1,2) corresponds to (4,1,1) in ECLIPSE. 

    We need to flip the z-components, as 
    Firedrake uses a mesh with z-direction upward.
    """
    arr = np.loadtxt(fin)
    arr[arr < minval] = minval
    arr[arr > maxval] = maxval

    arr = arr.flatten(order='C')
    arr = arr.reshape(dims, order='F')
    arr = arr[..., ::-1]

    np.save(fout, arr)
    return arr



# Example for SPE10
dims = (60, 220, 85)

minvals = (1e-1, 1e-1, 1e-2, 1e-2)
maxvals = (1e10, 1e10, 1e10, 1e10)
fins = ('KX_85.grdecl', 'KY_85.grdecl',
        'KZ_85.grdecl', 'PO_85.grdecl')
fouts = ('spe10_kx', 'spe10_ky', 'spe10_kz', 'spe10_po')

for i in range(4):
    ecl2npy(fins[i], fouts[i], dims, minvals[i], maxvals[i])
