import firedrake
from threephase import ThreePhase


class PressureDiff(ThreePhase):
    def __init__(self):
        k_x = firedrake.Constant(1)
        permeability = (k_x, k_x, k_x)
        mesh = self._generate_mesh()
        ICexpr = self._initial_conditions()

        super(PressureDiff, self).__init__(mesh, ICexpr, permeability)

        self.T = 20
        self.dtmax = 5

    def _generate_mesh(self):
        Delta_x = 20; Delta_y = 10; Delta_z = 2
        Nx = 2; Ny = 1;  Nz = 1
        Lx = Nx*Delta_x; Ly = Ny*Delta_y; Lz = Nz*Delta_z

        meshbase = firedrake.RectangleMesh(Nx, Ny, Lx, Ly, quadrilateral=True)
        mesh = firedrake.ExtrudedMesh(meshbase, Nz, Delta_z)

        self._Delta_x = Delta_x
        self._Delta_z = Delta_z

        return mesh

    def _initial_conditions(self):
        p01 = 250.0  # bar
        p02 = 200.0
        p0expr = "(x[0]<Dx) ? p01 : p02"

        So0expr = "(x[2]>Dz && x[2]<2*Dz) ? 1.0 : 0.0"
        Sg0expr = "(x[2]<Dz) ? 1.0 : 0.0"
        Sw0expr = "(x[2]>2*Dz) ? 1.0 : 0.0"
        So0expr = "1.0/3.0"
        Sg0expr = "1.0/3.0"
        Sw0expr = "1.0/3.0"

        bo0expr = "(x[0]<Dx) ? bo01 : bo02"
        bg0expr = "(x[0]<Dx) ? bg01 : bg02"
        bw0expr = "(x[0]<Dx) ? bw01 : bw02"

        mo0expr = "(" + bo0expr + ")*(" + So0expr + ")"
        mg0expr = "(" + bg0expr + ")*(" + Sg0expr + ")"
        mw0expr = "(" + bw0expr + ")*(" + Sw0expr + ")"

        bo01 = self.MolarDensity_o(p01)
        bg01 = self.MolarDensity_g(p01)
        bw01 = self.MolarDensity_w(p01)

        bo02 = self.MolarDensity_o(p02)
        bg02 = self.MolarDensity_g(p02)
        bw02 = self.MolarDensity_w(p02)

        return firedrake.Expression((mo0expr, mg0expr, mw0expr, p0expr),
                                    bo01=bo01, bg01=bg01, bw01=bw01, p01=p01,
                                    bo02=bo02, bg02=bg02, bw02=bw02, p02=p02,
                                    Dx=self._Delta_x, Dz=self._Delta_z)
