import firedrake
from threephase import ThreePhase


class ThreeCell(ThreePhase):
    _Delta_x = 20; _Delta_y = 10; _Delta_z = 2
    _Nx = 1; _Ny = 1; _Nz = 3

    _csv = True
    #_csv_eclijk = [[1,1,1], [1,1,2], [1,1,3]]

    def __init__(self):
        k_x = firedrake.Constant(100)
        permeability = (k_x, k_x, k_x)
        mesh = self._generate_mesh()
        ICexpr = self._initial_conditions()

        super(ThreeCell, self).__init__(mesh, ICexpr, permeability)

        self.T = 60
        self.dtmax = 5

    def _initial_conditions(self):
        # Initial conditions
        p0float = 250.0  # bar

        layer2 = self._Nz * 2/3 * self._Delta_z
        layer1 = self._Nz * 1/3 * self._Delta_z
        eps = 0.0

        So0expr = "(x[2]>layer1 && x[2]< layer2) ? 1.0-eps : 0.5*eps"
        Sg0expr = "(x[2]<layer1) ? 1.0-eps : 0.5*eps"
        Sw0expr = "(x[2]>layer2) ? 1.0-eps : 0.5*eps"

        mo0expr = "bo0*(" + So0expr + ")"
        mg0expr = "bg0*(" + Sg0expr + ")"
        mw0expr = "bw0*(" + Sw0expr + ")"

        bo0 = self.MolarDensity_o(p0float)
        bg0 = self.MolarDensity_g(p0float)
        bw0 = self.MolarDensity_w(p0float)

        return firedrake.Expression((mo0expr, mg0expr, mw0expr, "p0"),
                                    bo0=bo0, bg0=bg0, bw0=bw0, p0=p0float,
                                    layer1=layer1, layer2=layer2,
                                    eps=eps)
