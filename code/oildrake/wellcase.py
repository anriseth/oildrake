import firedrake
from threephase import ThreePhase
import wells
import numpy as np

class WellCase(ThreePhase):
    _Delta_x = 20; _Delta_y = 10; _Delta_z = 2
    _Nx = 3; _Ny = 1;  _Nz = 3

    def __init__(self):
        self._mesh = self._generate_mesh()

        self.__init_function_space__()
        #k_x = self._generate_permeability()
        #k_x = firedrake.interpolate(k_x, self._V)
        k_x = firedrake.Constant(100.0)
        permeability = (k_x, k_x, k_x)

        ICexpr = self._initial_conditions()

        b_sc_w = self.r_options['rho_sc_w'] / self.r_options['M_w']
        inijk = np.array([1,1,1])
        in_pos = (inijk - 0.5) * [self._Delta_x, self._Delta_y, self._Delta_z]
        injector = wells.waterinjector_point(in_pos, 20.0, b_sc_w)
        injector = firedrake.interpolate(injector, self._V)
        self._injectors = [injector]

        prodijk = np.array([self._Nx, self._Ny, self._Nz])
        prod_pos = (prodijk - 0.5) * [self._Delta_x, self._Delta_y, self._Delta_z]
        producer = {'pos': prod_pos, 'bhp': 200.0}
        self._producers = [producer]

        super(WellCase, self).__init__(self._mesh, ICexpr, permeability, pvd=False)

        self.dt = firedrake.Constant(0.1)
        self.T = 1.0
        self.dtmax = 0.1

    def _generate_mesh(self):
        Lx = self._Nx*self._Delta_x
        Ly = self._Ny*self._Delta_y

        meshbase = firedrake.RectangleMesh(self._Nx, self._Ny, Lx, Ly, quadrilateral=True)
        mesh = firedrake.ExtrudedMesh(meshbase, self._Nz, self._Delta_z)

        return mesh

    def _generate_permeability(self):
        high = 100.0
        low = high *1e-3
        rhs = (self._Nx-1)*self._Delta_x
        top = self._Nz * 2/3 * self._Delta_z
        bottom = self._Nz * 1/3 * self._Delta_z
        expr = "(x[0] < rhs && x[2]> bottom && x[2] < top) ? low : high"

        return firedrake.Expression(expr, Dx=self._Delta_x,
                                    rhs=rhs, top=top, bottom=bottom,
                                    low=low, high=high)

    def _initial_conditions(self):

        # Initial conditions
        p0float = 250.0  # bar

        layer2 = self._Nz * 2/3 * self._Delta_z
        layer1 = self._Nz * 1/3 * self._Delta_z
        eps = 0.0

        So0expr = "(x[2]>layer1 && x[2]< layer2) ? 1.0-eps : 0.5*eps"
        Sg0expr = "(x[2]>layer2) ? 1.0-eps : 0.5*eps"
        Sw0expr = "(x[2]<layer1) ? 1.0-eps : 0.5*eps"

        mo0expr = "bo0*(" + So0expr + ")"
        mg0expr = "bg0*(" + Sg0expr + ")"
        mw0expr = "bw0*(" + Sw0expr + ")"

        bo0 = self.MolarDensity_o(p0float)
        bg0 = self.MolarDensity_g(p0float)
        bw0 = self.MolarDensity_w(p0float)

        return firedrake.Expression((mo0expr, mg0expr, mw0expr, "p0"),
                                    bo0=bo0, bg0=bg0, bw0=bw0, p0=p0float,
                                    layer1=layer1, layer2=layer2,
                                    eps=eps)
