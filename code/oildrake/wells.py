from firedrake import Expression

EPS = 3e-16

nearx = "(x[0] <= wx + eps && x[0] >= wx - eps)"
neary = "(x[1] <= wy + eps && x[1] >= wy - eps)"
nearz = "(x[2] <= wz + eps && x[2] >= wz - eps)"

nearstr = "(" + nearx + " && " + neary + " && " + nearz + ")"


def waterinjector_point(w, rate, b_sc_w):
    expr = nearstr + " ? rate * b_sc_w : 0.0"
    return Expression(expr, wx=w[0], wy=w[1], wz=w[2],
                      rate=rate, b_sc_w=b_sc_w,
                      eps=EPS)
