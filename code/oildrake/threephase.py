from petsc4py import PETSc
from mpi4py import MPI
from firedrake import *
import numpy as np
import os
import matplotlib.pyplot as plt
from project_monitor import project_monitor
from cpr_preconditioner import CPRStage1
import utils

parameters["coffee"]["O2"] = False
parameters["matnest"] = False


class ThreePhase(object):

    #
    # Input values
    #
    r_options = dict()
    # Physical constants
    r_options['rho_sc_o'] = 800.0  # kg/m^3
    r_options['rho_sc_g'] = 0.9907  # kg/m^3
    r_options['rho_sc_w'] = 1022.0  # kg/m^3
    r_options['M_o'] = 120.0  # kg/kmol
    r_options['M_g'] = 25.0  # kg/kmol
    r_options['M_w'] = 18.025  # kg/kmol
    r_options['g'] = 9.80665e-5  # m^2kg/bar

    # Reservoir specific input values
    r_options['varphi'] = 0.3
    r_options['p_ref'] = 250.0  # bar
    r_options['B_pref_w'] = 1.03
    r_options['mu_pref_w'] = 0.3  # cP
    r_options['C_v'] = 0.0  # 1/bar
    r_options['C'] = 4.1e-5  # 1/bar
    r_options['C_rock'] = 5.3e-5  # 1/bar

    # Interpolation tables, oil and gas
    # pressure (bar), formation volume (-), viscosity (cP)
    r_options['oil_table'] = np.array([[50, 1.18, 0.8], [600, 1.08, 1.6]])
    r_options['gas_table'] = np.array([[50, 2.05e-2, 1.4e-2],
                                       [600, 3.9e-3, 2.5e-2]])

    # Unit conversion between accumulation and flow term
    # Because of ECLIPSE/INTERSECT not non-dimensionalising
    r_options['C_darcy'] = 8.52702e-3  # cPm^2/day/bar

    # EPS is used to prevent division by zero in rho_*_facet
    EPS = Constant(1e-12)

    # Flag deciding whether to use trueImpesDss or not
    # Change it before calling init_solver
    _useTrueImpes = False

    # Timestepping
    # Choose whether to use an adaptive timestepping or not
    adaptive_dt = True
    dt = Constant(0.1)
    dtmax = 50.0
    dtmin = 0.02
    dtmaxinc = 2.0  # dt max increase factor
    dtmaxdec = 0.5  # dt max decrease factor
    # Maximum predicted component saturation change for timestep.
    DSmax = 0.2  # ECLIPSE default 0.2
    T = float(dt)

    # Solution recording
    _csv = False  # Default: don't write solutions to csv
    _csv_eclijk = None
    _csv_dofs = None  # Default: print all cells to csv
    _pvd = False  # Default: don't write solutions to pvd

    @property
    def csv(self):
        return self._csv

    @csv.setter
    def csv(self, val):
        assert type(val) is bool, 'Must pass a boolean value to _csv'
        self._csv = val
        self.__init_datafiles__()

    @property
    def pvd(self):
        return self._pvd

    @pvd.setter
    def pvd(self, val):
        assert type(val) is bool, 'Must pass a boolean value to _pvd'
        self._pvd = val
        self.__init_datafiles__()

    def __init__(self, mesh, ICexpr, permeability):
        self.comm = mesh.comm
        self._name = type(self).__name__

        self._csv_dofs = utils.ecl2dofs(self._csv_eclijk, self._Ny, self._Nz)

        self._mesh = mesh
        self._ICexpr = ICexpr

        self.k_x = permeability[0]
        self.k_y = permeability[1]
        self.k_z = permeability[2]

        try:
            self._W
        except AttributeError:
            self.__init_function_space__()

        try:
            self._injectors
        except AttributeError:
            self._injectors = list()

        try:
            self._producers
        except AttributeError:
            self._producers = list()

        # Solver parameters
        self.__init_solver_params__()


        # Set up variational form to self.F
        self.__init_variational_form__()

        # Init filenames for testproblem
        self.__init_datafiles__()

    def _generate_mesh(self, Delta_x=None, Delta_y=None, Delta_z=None,
                       Nx=None, Ny=None, Nz=None):
        Delta_x = Delta_x or self._Delta_x
        Delta_y = Delta_y or self._Delta_y
        Delta_z = Delta_z or self._Delta_z
        Nx = Nx or self._Nx
        Ny = Ny or self._Ny
        Nz = Nz or self._Nz

        Lx = Nx*Delta_x; Ly = Ny*Delta_y

        meshbase = RectangleMesh(Nx, Ny, Lx, Ly, quadrilateral=True)
        mesh = ExtrudedMesh(meshbase, Nz, Delta_z)

        # Need these to convert function to permeability field
        self._Delta_x = Delta_x; self._Delta_y = Delta_y; self._Delta_z = Delta_z

        return mesh

    def _ic_inversion(self, layer1=None, layer2=None, p0float=250, eps=0.0):
        layer1 = layer1 or 1.0/3.0 * self._Nz * self._Delta_z
        layer2 = layer2 or 2.0/3.0 * self._Nz * self._Delta_z

        So0expr = "(x[2]>layer1 && x[2]< layer2) ? 1.0-eps : 0.5*eps"
        Sg0expr = "(x[2]<layer1) ? 1.0-eps : 0.5*eps"
        Sw0expr = "(x[2]>layer2) ? 1.0-eps : 0.5*eps"

        mo0expr = "bo0*(" + So0expr + ")"
        mg0expr = "bg0*(" + Sg0expr + ")"
        mw0expr = "bw0*(" + Sw0expr + ")"

        bo0 = self.MolarDensity_o(p0float)
        bg0 = self.MolarDensity_g(p0float)
        bw0 = self.MolarDensity_w(p0float)

        return Expression((mo0expr, mg0expr, mw0expr, "p0"),
                          bo0=bo0, bg0=bg0, bw0=bw0, p0=p0float,
                          layer1=layer1, layer2=layer2,
                          eps=eps)

    def __init_datafiles__(self):
        # Name for data files
        self._datadir = './data'
        folder = self._datadir

        try:
            os.mkdir(folder)
        except OSError:
            pass

        tname = folder + '/time-%s.' % self._name
        pname = folder + '/pressure-%s.' % self._name
        oname = folder + '/oil-%s.' % self._name
        gname = folder + '/gas-%s.' % self._name
        wname = folder + '/water-%s.' % self._name

        if self._csv:
            self._tfile = tname + 'csv'
            self._pfile = pname + 'csv'
            self._ofile = oname + 'csv'
            self._gfile = gname + 'csv'
            self._wfile = wname + 'csv'

        if self._pvd:
            self._ppvd = File(pname + 'pvd')
            self._opvd = File(oname + 'pvd')
            self._gpvd = File(gname + 'pvd')
            self._wpvd = File(wname + 'pvd')

    def __init_function_space__(self):
        #
        # Set up function space
        #
        horiz_elt = FiniteElement("DG", quadrilateral, 0)
        vert_elt = FiniteElement("DG", interval, 0)
        elt = TensorProductElement(horiz_elt, vert_elt)
        self._V = FunctionSpace(self._mesh, elt)
        self._W = MixedFunctionSpace([self._V, self._V, self._V, self._V])

    def __init_solver_params__(self):
        # TODO: Should these just be set as static?
        #
        # Solver parameters
        #

        maxiter = 20  # ECLIPSE default: 20. Intersect: 12

        snes_params = {
            'snes_atol': 1.0e-3,
            'snes_rtol': 1.0e-8,
            'snes_stol': 0.0,
            'snes_max_linear_solve_fail': maxiter,
            'snes_max_it': maxiter,
            'snes_linesearch_type': 'basic',
            'snes_type': 'newtonls',
        }

        ksp_params = {
            'ksp_atol': 1e-10,
            'ksp_rtol': 1e-5,
        }

        ksp_lu = {
            'ksp_type': 'preonly',
            'pc_type': 'lu',
            'pc_factor_mat_solver_package': 'mumps',
        }

        ksp_gmres = {
            'ksp_type': 'gmres',
            'pc_type': 'ilu',
        }

        ksp_cpr = {
            'ksp_type': 'fgmres',
            'ksp_max_it': 20,  # ECLIPSE and IX default: 20
            'pc_type': 'composite',
            'pc_composite_type': 'multiplicative',
            'pc_composite_pcs': 'python,ilu',
            'cpr_stage1_pc_type': 'hypre',
            'cpr_stage1_pc_hypre_type': 'boomeramg'
        }

        params = snes_params.copy()
        params.update(ksp_params)
        #params.update(ksp_lu)
        #params.update(ksp_gmres)
        params.update(ksp_cpr)

        # Fix as nest=False no longer part of NonlinearVariationalProblem
        params.update({'mat_type': 'aij'})

        self.solver_parameters = params

    def init_solver(self, ops_prefix=None):
        # TODO: separate out? Make classmethod?
        ops_prefix = ops_prefix or 'od_'
        # Does loads of compiling?
        self.problem = NonlinearVariationalProblem(self.F, self.w_next)
        solver = NonlinearVariationalSolver(self.problem, options_prefix=ops_prefix,
                                            solver_parameters=self.solver_parameters)
        sneses = [solver.snes]

        for snes in sneses:
            if snes.hasNPC():
                # Need to call setup to init the NPC snes
                snes.setUp()
                sneses.append(snes.npc)

            if snes.getType() == 'composite':
                num_sneses = snes.getCompositeNumber()
                sneses.extend([snes.getCompositeSNES(subsnes) for
                               subsnes in range(num_sneses)])
                continue
            elif snes.getType() in ('vinewtonrsls', 'vinewtonssls'):
                lb = Function(self._W)
                ub = Function(self._W)
                with lb.dat.vec_ro as lbvec, ub.dat.vec as ubvec:
                    ubvec.set(PETSc.INFINITY)
                    snes.setVariableBounds(lbvec, ubvec)
            elif snes.type == 'ngmres' and snes.hasNPC():
                    warning(("Not setting project_monitor on '%s'. "
                             "Left it to the NPC: '%s'")
                            % (snes.type, snes.npc.type))
            else:
                petsc_debug = False
                if petsc_debug:
                    info_red("Running petsc in debugging mode.")
                    info_red("project_monitor not set.")
                else:
                    snes.setMonitor(project_monitor)

            if snes.ksp.pc.getType() == 'composite':
                subpc = snes.ksp.pc.getCompositePC(0)
                cpr_stage1 = self._create_cpr_stage_1(snes, ops_prefix)
                subpc.setPythonContext(cpr_stage1)
                # if subpc.type == 'python':
                #     cpr_stage1 = self._create_cpr_stage_1(snes, ops_prefix)
                #     subpc.setPythonContext(cpr_stage1)
                # elif subpc.type == 'fieldsplit':
                #     # TODO: should have been fixed in mapdes petsc4py?
                #     fdofs = self._W.dof_dset.field_ises
                #     s_is = PETSc.IS().createGeneral(np.concatenate([iset.indices for iset in fdofs[:-1]]))
                #     p_is = fdofs[-1]
                #     fields = [("0", s_is), ("1", p_is)]
                #     subpc.setFieldSplitIS(*fields)
                # else:
                #     info_red("Using PCComposite without python/fieldsplit as first pc")

        self.solver = solver

    def _create_cpr_stage_1(self, snes, ops_prefix):
        # Set fieldsplit indices
        fdofs = self._W.dof_dset.field_ises  # TODO: should this be local_ises?
        s_is = PETSc.IS().createGeneral(np.concatenate(
            [iset.indices for iset in fdofs[:-1]]))
        p_is = fdofs[-1]
        cpr_stage1 = CPRStage1(snes, s_is, p_is, self._useTrueImpes,
                               ops_prefix=ops_prefix)
        return cpr_stage1

    """
    Variational form
    """
    def __init_variational_form__(self):
        V = self._V
        W = self._W

        self.w_next = Function(W, name="Next")
        self.w_old = Function(W, name="Old")

        (mo_next, mg_next, mw_next, p_next) = split(self.w_next)
        (mo_old, mg_old, mw_old, p_old) = split(self.w_old)

        (v_o, v_g, v_w, v_S) = TestFunctions(W)

        #
        # Define coordinate functions
        #
        x_func_expr = Expression("x[0]")
        y_func_expr = Expression("x[1]")
        z_func_expr = Expression("x[2]")

        x_func = interpolate(x_func_expr, V)
        y_func = interpolate(y_func_expr, V)
        z_func = interpolate(z_func_expr, V)
        Delta_h_facet = sqrt(jump(x_func)**2
                             + jump(y_func)**2
                             + jump(z_func)**2)

        S_o_next = self.Saturation_o(mo_next, p_next)
        S_g_next = self.Saturation_g(mg_next, p_next)
        S_w_next = self.Saturation_w(mw_next, p_next)

        lmbda_o_next = self.Mobility_o(mo_next, p_next)
        lmbda_g_next = self.Mobility_g(mg_next, p_next)
        lmbda_w_next = self.Mobility_w(mw_next, p_next)

        rho_o_next = self.MassDensity_o(p_next)
        rho_g_next = self.MassDensity_g(p_next)
        rho_w_next = self.MassDensity_w(p_next)

        g = Constant(self.r_options['g'])

        phi_old = self.Porosity(p_old)
        phi_next = self.Porosity(p_next)

        # On facets
        # TODO: Division by zero problems.
        # Is there a better way to approach this?
        rho_o_sat = (S_o_next('-')*rho_o_next('-') +
                     S_o_next('+')*rho_o_next('+')) / \
            (S_o_next('-') + S_o_next('+') + self.EPS)
        rho_o_next_facet = conditional(eq(avg(S_o_next), 0.0), avg(rho_o_next),
                                       rho_o_sat)
        rho_g_sat = (S_g_next('-')*rho_g_next('-') +
                     S_g_next('+')*rho_g_next('+')) / \
            (S_g_next('-') + S_g_next('+') + self.EPS)
        rho_g_next_facet = conditional(eq(avg(S_g_next), 0.0), avg(rho_g_next),
                                       rho_g_sat)
        rho_w_sat = (S_w_next('-')*rho_w_next('-') +
                     S_w_next('+')*rho_w_next('+')) / \
            (S_w_next('-') + S_w_next('+') + self.EPS)
        rho_w_next_facet = conditional(eq(avg(S_w_next), 0.0), avg(rho_w_next),
                                       rho_w_sat)

        k_x = self.k_x
        k_y = self.k_y
        k_z = self.k_z

        k_x_facet = conditional(gt(avg(k_x), 0.0), k_x('+')*k_x('-') / avg(k_x), 0.0)
        k_y_facet = conditional(gt(avg(k_y), 0.0), k_y('+')*k_y('-') / avg(k_y), 0.0)
        k_z_facet = conditional(gt(avg(k_z), 0.0), k_z('+')*k_z('-') / avg(k_z), 0.0)

        dp_next_facet = jump(p_next) / Delta_h_facet

        # Terms on vertical faces (ie in x,y-direction)
        flowterm_v_o_next = dp_next_facet
        flowterm_v_g_next = dp_next_facet
        flowterm_v_w_next = dp_next_facet

        mobility_v_o_next_facet = conditional(lt(flowterm_v_o_next, 0),
                                              lmbda_o_next('-'), lmbda_o_next('+'))
        mobility_v_g_next_facet = conditional(lt(flowterm_v_g_next, 0),
                                              lmbda_g_next('-'), lmbda_g_next('+'))
        mobility_v_w_next_facet = conditional(lt(flowterm_v_w_next, 0),
                                              lmbda_w_next('-'), lmbda_w_next('+'))

        # Terms on horizontal faces (ie in z-direction)
        flowterm_h_o_next = dp_next_facet - rho_o_next_facet*g
        flowterm_h_g_next = dp_next_facet - rho_g_next_facet*g
        flowterm_h_w_next = dp_next_facet - rho_w_next_facet*g

        mobility_h_o_next_facet = conditional(lt(flowterm_h_o_next, 0),
                                              lmbda_o_next('-'), lmbda_o_next('+'))
        mobility_h_g_next_facet = conditional(lt(flowterm_h_g_next, 0),
                                              lmbda_g_next('-'), lmbda_g_next('+'))
        mobility_h_w_next_facet = conditional(lt(flowterm_h_w_next, 0),
                                              lmbda_w_next('-'), lmbda_w_next('+'))

        F_S = (1.0 - S_o_next - S_g_next - S_w_next)*v_S*dx
        F_t = (phi_next*mo_next - phi_old*mo_old)*v_o*dx + \
              (phi_next*mg_next - phi_old*mg_old)*v_g*dx + \
              (phi_next*mw_next - phi_old*mw_old)*v_w*dx

        n = FacetNormal(self._mesh)

        # This is a hack to make K*n work in parallel
        k_x_flow = k_x_facet*(abs(n[0]('-'))+abs(n[0]('+')))/2
        k_y_flow = k_y_facet*(abs(n[1]('-'))+abs(n[1]('+')))/2

        F_v_o = (k_y_flow+k_x_flow) * mobility_v_o_next_facet * \
                flowterm_v_o_next * jump(v_o) * dS_v

        F_h_o = k_z_facet * mobility_h_o_next_facet * \
                flowterm_h_o_next * jump(v_o) * dS_h
        F_o = F_h_o + F_v_o

        F_v_g = (k_y_flow+k_x_flow) * mobility_v_g_next_facet * \
                flowterm_v_g_next * jump(v_g) * dS_v
        F_h_g = k_z_facet * mobility_h_g_next_facet * \
                flowterm_h_g_next * jump(v_g) * dS_h
        F_g = F_h_g + F_v_g

        F_v_w = (k_y_flow+k_x_flow) * mobility_v_w_next_facet * \
                flowterm_v_w_next * jump(v_w) * dS_v
        F_h_w = k_z_facet * mobility_h_w_next_facet * \
                flowterm_h_w_next * jump(v_w) * dS_h
        F_w = F_h_w + F_v_w

        F_flow = F_o + F_g + F_w

        C_darcy = Constant(self.r_options['C_darcy'])

        self.F = F_S + (F_t + self.dt*C_darcy*F_flow)

        cellvolume = Constant(self._Delta_x * self._Delta_y * self._Delta_z)

        # Water injectors
        # TODO: rewrite injectors, don't want expressions?
        # - Or should we do expressions for injectors as well?
        for source in self._injectors:
            self.F = self.F - self.dt/cellvolume*source*v_w*dx

        # Producers
        point_in_cell = """
        void point_in_cell(double *out[1], double **coords,
                           double *point)
        {
          int i, incell = 1;
          int extrema[3] = {4, 2, 1};
          for (i = 0; i<3; i++) {
            incell = incell && (coords[0][i] <= point[i]) && (point[i] < coords[extrema[i]][i]);
          }
          if (incell) {
            out[0][0] = 1.0;
          }
        }
        """
        kernel = op2.Kernel(point_in_cell, "point_in_cell")
        Vvec = VectorFunctionSpace(self._mesh, "DG", 0)
        coords = self._mesh.coordinates
        for prod in self._producers:
            point = op2.Global(3, data=prod['pos'])
            bhp = Constant(prod['bhp'])
            delta = Function(self._V)
            op2.par_loop(kernel, delta.cell_set,
                         delta.dat(op2.WRITE, delta.cell_node_map()),
                         coords.dat(op2.READ, coords.cell_node_map()),
                         point(op2.READ))
            dd = max_value(p_next - bhp, 0.0)
            outflow = delta*dd*(lmbda_o_next*v_o + lmbda_g_next*v_g
                                + lmbda_w_next*v_w)
            self.F = self.F + self.dt/cellvolume*outflow*dx

    def _write_csv(self, first=False):
        # TODO: use try / except instead of parameter first?
        if first:
            self._thandle = open(self._tfile, 'w')
            self._phandle = open(self._pfile, 'w')
            self._ohandle = open(self._ofile, 'w')
            self._ghandle = open(self._gfile, 'w')
            self._whandle = open(self._wfile, 'w')

        dofs = self._csv_dofs
        if dofs is None:
            dofs = np.arange(self._V.node_count)
        t = self._t

        np.savetxt(self._thandle, (t,), delimiter=',')
        np.savetxt(self._ohandle, self.Svec_o_next[dofs][None],
                   delimiter=',')
        np.savetxt(self._ghandle, self.Svec_g_next[dofs][None],
                   delimiter=',')
        np.savetxt(self._whandle, self.Svec_w_next[dofs][None],
                   delimiter=',')
        np.savetxt(self._phandle, self.w_next.sub(3).dat.data_ro[dofs][None],
                   delimiter=',')

    def _write_pvd(self):
        # TODO: File projects to DG1 each time - not really feasible
        (mo, mg, mw, p) = self.w_next.split()
        self._ppvd << (p, self._t)
        self._opvd << (mo, self._t)
        self._gpvd << (mg, self._t)
        self._wpvd << (mw, self._t)

    def write_sol(self, first=False):
        with PETSc.Log().Event('Write Solution') as event:
            if self._csv:
                self._write_csv(first)

            if self._pvd:
                self._write_pvd()

    def close_files(self):
        self._thandle.close()
        self._phandle.close()
        self._ohandle.close()
        self._ghandle.close()
        self._whandle.close()

    def values(self, idx=None):
        '''
        Get the solution values from generated csv files
        '''
        tarr = np.loadtxt(self._tfile, delimiter=',')
        parr = np.loadtxt(self._pfile, delimiter=',')
        oarr = np.loadtxt(self._ofile, delimiter=',')
        garr = np.loadtxt(self._gfile, delimiter=',')
        warr = np.loadtxt(self._wfile, delimiter=',')

        if idx is not None:
            parr = parr[:, idx]
            oarr = oarr[:, idx]
            garr = garr[:, idx]
            warr = warr[:, idx]

        return (tarr, oarr, garr, warr, parr)

    def plotresults(self, idx=None, legend=True):
        # TODO: make this a classmethod? [And separate it out from this file?]
        (tarr, oarr, garr, warr, parr) = self.values(idx)
        f, axarr = plt.subplots(2, 2, sharex='col')#, sharey='row')

        l00 = axarr[0, 0].plot(tarr, parr)
        axarr[0, 1].plot(tarr, oarr)
        axarr[1, 0].plot(tarr, garr)
        axarr[1, 1].plot(tarr, warr)

        axarr[0, 0].set_ylabel('Pressure (bar)')
        axarr[0, 1].set_ylabel('Oil saturation')
        axarr[1, 0].set_ylabel('Gas saturation')
        axarr[1, 1].set_ylabel('Water saturation')
        axarr[1, 1].set_xlabel('Time (days)')
        axarr[1, 0].set_xlabel('Time (days)')

        axarr[1, 0].set_xlim([tarr.min(), tarr.max()])
        axarr[1, 1].set_xlim([tarr.min(), tarr.max()])
        axarr[0, 1].set_ylim([0, 1])
        axarr[1, 0].set_ylim([0, 1])
        axarr[1, 1].set_ylim([0, 1])

        if legend and self._csv_eclijk is not None:
            eclipseids = self._csv_eclijk
            legends = [str(eclipseids[i])
                       for i in range(len(eclipseids))]
            f.legend(l00, legends,
                     loc='upper right', title='ECLIPSE ijk')

        plt.suptitle("%s solution for selected cells" % self._name)
        plt.show()

    def calculate_saturation_vecs(self, w=None):
        w = w or self.w_next
        movec, mgvec, mwvec, pvec = w.dat.data_ro
        Svec_o = self.Saturation_o(movec, pvec)
        Svec_g = self.Saturation_g(mgvec, pvec)
        Svec_w = self.Saturation_w(mwvec, pvec)
        return (Svec_o, Svec_g, Svec_w)

    def _calculate_sctfactor(self, DSmax=None):
        '''
        Similar to ECLIPSE/INTERSECT:
        Assume change in saturation at previous timestep
        will be similar at next timestep.
        Use this to decide how large we can set the next timestep.
        Absolute/normalised changes summed over each component for every cell
        '''
        DSmax = DSmax or self.DSmax

        # TODO: make indep of self? Put in
        DS_o = np.abs(self.Svec_o_next-self.Svec_o_old)
        DS_g = np.abs(self.Svec_g_next-self.Svec_g_old)
        DS_w = np.abs(self.Svec_w_next-self.Svec_w_old)

        lDS = np.max(DS_o + DS_g + DS_w)  # Local to processor
        DS = self.comm.allreduce(lDS, op=MPI.MAX)

        if DS == 0:
            sctfactor = self.dtmaxinc
        else:
            sctfactor = DSmax / DS
        return sctfactor

    def _update_dt(self):
        sctfactor = self._calculate_sctfactor()
        dt = float(self.dt)
        newdt = utils.update_dt(dt, self.dtmin, self.dtmax,
                                self.dtmaxdec, self.dtmaxinc,
                                sctfactor)
        self.dt.assign(newdt)

    def solve(self):
        w_old = self.w_old
        w_next = self.w_next
        w_next.interpolate(self._ICexpr)
        w_old.interpolate(self._ICexpr)

        # Initial calculation of saturations
        # Used for reporting and adaptive timestepping
        (self.Svec_o_old, self.Svec_g_old,
         self.Svec_w_old) = self.calculate_saturation_vecs(w_old)

        t = 0.0
        self._t = t

        (self.Svec_o_next, self.Svec_g_next,
         self.Svec_w_next) = (self.Svec_o_old, self.Svec_g_old,
                              self.Svec_w_old)
        self.write_sol(first=True)

        failures = 0
        maxfailures = 20
        self.timestep = 0

        while t < self.T-float(self.EPS):
            newt = min(t + float(self.dt), self.T)
            self.dt.assign(newt-t)
            t = newt
            self._t = t

            try:
                # TODO: make all this exception handling more smooth / unified
                self.solver.solve()
            except PETSc.Error as e:
                failures += 1
                if failures > maxfailures:
                    if self.comm.rank == 0:
                        print "ERROR: Reached maximum number of newton failures: %d" % maxfailures
                    raise e

                if self.comm.rank == 0:
                    print "PETSc Error: " + str(e)

                t -= float(self.dt)
                self.dt.assign(0.5*float(self.dt))
                w_next.assign(w_old)
                self.init_solver()
                continue
            except RuntimeError as e:
                failures += 1
                if failures > maxfailures:
                    if self.comm.rank == 0:
                        print "ERROR: Reached maximum number of newton failures: %d" % maxfailures
                    raise e

                r = self.solver.snes.getConvergedReason()
                if r < 0:
                    if self.comm.rank == 0:
                        print "RuntimeError: " + str(e)
                        print "Halving time-step ..."

                    t -= float(self.dt)
                    self.dt.assign(0.5*float(self.dt))
                    w_next.assign(w_old)
                    self.init_solver()
                    continue
                else:
                    raise e

            w_old.assign(w_next)

            ## Solution update finished, do some admin:

            # Time step updating heuristic
            # TODO: use this for benchmarking?
            numiter = self.solver.snes.its
            self.timestep += 1

            '''
            Calculate saturations from solution to support:
             - Printing to csv
             - adaptive timestepping: self._calculate_sctfactor()
            '''
            (self.Svec_o_next, self.Svec_g_next,
             self.Svec_w_next) = self.calculate_saturation_vecs(w_next)

            if self.adaptive_dt is True:
                self._update_dt()

            # Prepare for next iteration
            self.Svec_o_old = self.Svec_o_next
            self.Svec_g_old = self.Svec_g_next
            self.Svec_w_old = self.Svec_w_next

            # Print info
            if self.comm.rank == 0:
                print("t = %f\t newdt = %f\t iter = %d\t"
                      % (t, float(self.dt), numiter))

            # Save solutions to file
            self.write_sol()

        if self._csv:
            self.close_files()


    """
    Relations between reservoir and fluid properties
    """
    def FormationVolume_o(self, p):
        pvals = self.r_options['oil_table'][:, 0]
        Bvals = self.r_options['oil_table'][:, 1]

        coeff = (Bvals[1]-Bvals[0]) / (pvals[1]-pvals[0])
        return Bvals[0] + (p - pvals[0]) * coeff

    def FormationVolume_g(self, p):
        pvals = self.r_options['gas_table'][:, 0]
        Bvals = self.r_options['gas_table'][:, 1]

        coeff = (Bvals[1]-Bvals[0]) / (pvals[1]-pvals[0])
        return Bvals[0] + (p - pvals[0]) * coeff

    def FormationVolume_w(self, p):
        X = self.r_options['C'] * (p - self.r_options['p_ref'])

        return self.r_options['B_pref_w'] / (1.0 + X + 0.5*X**2)

    def Viscosity_o(self, p):
        pvals = self.r_options['oil_table'][:, 0]
        muvals = self.r_options['oil_table'][:, 2]

        coeff = (muvals[1]-muvals[0]) / (pvals[1]-pvals[0])
        return muvals[0] + (p - pvals[0]) * coeff

    def Viscosity_g(self, p):
        pvals = self.r_options['gas_table'][:, 0]
        muvals = self.r_options['gas_table'][:, 2]

        coeff = (muvals[1]-muvals[0]) / (pvals[1]-pvals[0])
        return muvals[0] + (p - pvals[0]) * coeff

    def Viscosity_w(self, p):
        X = self.r_options['C_v'] * (p - self.r_options['p_ref'])

        return self.r_options['mu_pref_w'] / (1 + X + 0.5*X**2)

    def MolarDensity_o(self, p):
        b_sc_o = self.r_options['rho_sc_o'] / self.r_options['M_o']
        B_o = self.FormationVolume_o(p)
        return b_sc_o / B_o

    def MolarDensity_g(self, p):
        b_sc_g = self.r_options['rho_sc_g'] / self.r_options['M_g']
        B_g = self.FormationVolume_g(p)

        return b_sc_g / B_g

    def MolarDensity_w(self, p):
        b_sc_w = self.r_options['rho_sc_w'] / self.r_options['M_w']
        B_w = self.FormationVolume_w(p)
        return b_sc_w / B_w

    def Saturation_o(self, m, p):
        b_o = self.MolarDensity_o(p)

        return m / b_o

    def Saturation_g(self, m, p):
        b_g = self.MolarDensity_g(p)

        return m / b_g

    def Saturation_w(self, m, p):
        b_w = self.MolarDensity_w(p)

        return m / b_w

    def MassDensity_o(self, p):
        B_o = self.FormationVolume_o(p)

        return self.r_options['rho_sc_o'] / B_o

    def MassDensity_g(self, p):
        B_g = self.FormationVolume_g(p)

        return self.r_options['rho_sc_g'] / B_g

    def MassDensity_w(self, p):
        B_w = self.FormationVolume_w(p)

        return self.r_options['rho_sc_w'] / B_w

    def RelativePermeability_o(self, m, p):
        return self.Saturation_o(m, p)

    def RelativePermeability_g(self, m, p):
        return self.Saturation_g(m, p)

    def RelativePermeability_w(self, m, p):
        return self.Saturation_w(m, p)

    def Porosity(self, p):
        # self.varphi adds support for heterogeneous porosity fields
        try:
            retval = self.varphi
        except AttributeError:
            retval = self.r_options['varphi']

        X = Constant(self.r_options['C_rock']) * (p - Constant(self.r_options['p_ref']))
        return retval * (1.0 + X + 0.5*X**2)

    def Mobility_o(self, m, p):
        return self.MolarDensity_o(p) \
            * self.RelativePermeability_o(m, p) / self.Viscosity_o(p)

    def Mobility_g(self, m, p):
        return self.MolarDensity_g(p) \
            * self.RelativePermeability_g(m, p) / self.Viscosity_g(p)

    def Mobility_w(self, m, p):
        return self.MolarDensity_w(p) \
            * self.RelativePermeability_w(m, p) / self.Viscosity_w(p)
