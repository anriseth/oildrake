import numpy as np

def update_dt(dt, dtmin, dtmax, dtmaxdec, dtmaxinc, factor):
    # TODO: support factor being iterable
    dtfactor = max(dtmaxdec, min(dtmaxinc, factor))
    newdt = dt*dtfactor
    newdt = max(dtmin, min(dtmax, newdt))
    return newdt


'''
Translate ECLIPSE ijk convention to Python and Firedrake
'''


def ecl2py(eclijk, nz):
    if eclijk is None:
        return None

    # 0-based
    pythonids = eclijk - 1
    # invert z-direction
    pythonids[2] = nz - (pythonids[2] + 1)

    return pythonids


def ecl2dofs_scalar(eclijk, ny, nz):
    if eclijk is None:
        return None

    pythonids = ecl2py(eclijk, nz)
    dofid = nz*ny*pythonids[0] + \
            nz*pythonids[1] + \
            pythonids[2]

    return dofid


def ecl2dofs(eclijk, ny, nz):
    if eclijk is None:
        return None
    return np.apply_along_axis(ecl2dofs_scalar, 1, eclijk, ny, nz)
