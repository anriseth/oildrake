import firedrake
from threephase import ThreePhase
import numpy as np
import os
import utils


class SPE10Small(ThreePhase):
    _Delta_x = 20; _Delta_y = 10; _Delta_z = 2
    _Nx = 10; _Ny = 10; _Nz = 10
    _perm_file = os.path.join(os.path.dirname(__file__), 'reservoir_input/perm_spe10_small.npy')

    _csv = True
    _csv_eclijk = [[4,4,3], [4,4,5], [4,4,8],
                   [8,1,9], [3,7,2]]
    _csv_dofs = utils.ecl2dofs(_csv_eclijk, _Ny, _Nz)

    def __init__(self):
        self._name = 'SPE10Small'

        self._mesh = self._generate_mesh()
        layer1 = 3.0*self._Delta_z
        layer2 = 7.0*self._Delta_z
        self._ICexpr = self._ic_inversion(layer1=layer1, layer2=layer2)

        self.comm = self._mesh.comm
        self.__init_function_space__()
        self.__init_perm_field__()
        self.__init_solver_params__()

        # Timestepping
        self.dt = firedrake.Constant(0.1)
        self.dtmax = 50.0
        self.T = 365*3
        self.adaptive_dt = True

        self._injectors = []
        self._producers = []
        # Set up variational form to self.F
        self.__init_variational_form__()

        # datafiles for saving progress
        self.__init_datafiles__()

    def __init_perm_field__(self):
        Delta = np.array([self._Delta_x, self._Delta_y, self._Delta_z])
        Vvec = firedrake.VectorFunctionSpace(self._mesh, "DG", 0)
        coords = firedrake.project(self._mesh.coordinates, Vvec).dat.data
        perm_array = np.load(self._perm_file)

        self.k_x = firedrake.Function(self._V)

        @np.vectorize
        def coordtoijk(x, y, z):
            i = np.floor(x / Delta[0]).astype(int)
            j = np.floor(y / Delta[1]).astype(int)
            k = np.floor(z / Delta[2]).astype(int)
            return perm_array[i, j, k]

        self.k_x.dat.data[...] = coordtoijk(coords[:, 0], coords[:, 1], coords[:, 2])
        self.k_y = self.k_x
        self.k_z = self.k_x
