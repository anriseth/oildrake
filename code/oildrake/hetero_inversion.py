import firedrake
from threephase import ThreePhase
import numpy as np
from generate_perm_field import gen_perm_field
import utils


class HeteroInversion(ThreePhase):
    _Nx = 24; _Ny = 24; _Nz = 24
    _totX = 480.0; _totY = 240.0; _totZ = 48.0
    _Delta_x = _totX/_Nx; _Delta_y = _totY/_Ny; _Delta_z = _totZ/_Nz
    seedval = 0
    pminval, pmaxval = (10, 1000)
    pox, poy, poz = (3,3,6)
    origin = (_totX/3, _totY*0.8, -_totZ*0.1)

    _csv = True
    _csv_eclijk = [[8,8,8], [8,8,11], [8,8,15],
                   [20,15,13], [3,22,18]]
    _csv_dofs = utils.ecl2dofs(_csv_eclijk, _Ny, _Nz)

    def __init__(self):
        self._name = type(self).__name__

        self._mesh = self._generate_mesh()
        self._ICexpr = self._ic_inversion()

        self.comm = self._mesh.comm
        self.__init_function_space__()
        self.__init_perm_field__()
        self.__init_solver_params__()

        # Timestepping
        self.dt = firedrake.Constant(0.1)
        #self.dtmax = 50.0
        self.T = 200.0

        self._injectors = []
        self._producers = []
        # Set up variational form to self.F
        self.__init_variational_form__()

        # datafiles for saving progress
        self.__init_datafiles__()

    def __init_perm_field__(self):
        Delta = np.array([self._Delta_x, self._Delta_y, self._Delta_z])
        dims = np.array([self._Nx, self._Ny, self._Nz])
        Vvec = firedrake.VectorFunctionSpace(self._mesh, "DG", 0)
        coords = firedrake.project(self._mesh.coordinates, Vvec).dat.data
        perm_array = gen_perm_field(self.pminval, self.pmaxval,
                                    self.pox, self.poy, self.poz,
                                    dims, Delta, self.origin, self.seedval)

        self.k_x = firedrake.Function(self._V)

        @np.vectorize
        def coordtoijk(x, y, z):
            i = np.floor(x / Delta[0]).astype(int)
            j = np.floor(y / Delta[1]).astype(int)
            k = np.floor(z / Delta[2]).astype(int)
            return perm_array[i, j, k]

        self.k_x.dat.data[...] = coordtoijk(coords[:, 0], coords[:, 1], coords[:, 2])
        self.k_y = self.k_x
        self.k_z = self.k_x
