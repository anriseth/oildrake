from random import random, seed
from math import pi, sin, cos
import numpy as np


def gen_perm_field(minval, maxval, ox, oy, oz, dims, deltas, origin, seedval=None):
    seed(seedval)
    dx, dy, dz = (d for d in deltas)
    nx, ny, nz = (n for n in dims)
    lx, ly, lz = (dims[i]*deltas[i] for i in range(3))
    pfield = np.zeros(dims)

    for oi in range(1,ox):
        for oj in range(1,oy):
            for ok in range(1,oz):
                scoeffs, ccoeffs = (random(), random())
                xsscale, xcscale = (random()*2*pi / lx for i in range(2))
                ysscale, ycscale = (random()*2*pi / ly for i in range(2))
                zsscale, zcscale = (random()*2*pi / lz for i in range(2))
                for i in range(nx):
                    for j in range(ny):
                        for k in range(nz):
                            pfield[i,j,k] += scoeffs*sin(oi*(origin[0]+(0.5+i)*dx)*xsscale)\
                                             *sin(oj*(origin[1]+(0.5+j)*dy)*ysscale)\
                                             *sin(ok*(origin[2]+(0.5+k)*dz)*zsscale)
                            pfield[i,j,k] += ccoeffs*cos(oi*(origin[0]+(0.5+i)*dx)*xcscale)\
                                             *cos(oj*(origin[1]+(0.5+j)*dy)*ycscale)\
                                             *cos(ok*(origin[2]+(0.5+k)*dz)*zcscale)

    # Scale values in perm field between minval and maxval
    pmin, pmax = (pfield.min(), pfield.max())

    pfield = minval + (maxval-minval) * (pfield-pmin) / (pmax-pmin)
    return pfield
