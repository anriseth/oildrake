from petsc4py import PETSc

def project_monitor(snes, its, norm):
    if its == 0:
        return
    with PETSc.Log().Event('Project') as event:
        v = snes.getSolution()
        vloc = v.getArray()

        for i, val in enumerate(vloc):
            if val < 0.0:
                vloc[i] = 0.0
