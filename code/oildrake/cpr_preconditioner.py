# coding: utf-8

'''
Author: Asbjørn Nilsen Riseth, 2015

A PETSc CPR preconditioner set up to work with
the ECLIPSE E300 isothermal model with no wells.

Supports Quasi and True Impes, although the True Impes version
is probably much slower than it can be
'''

'''
# Usage:
1. Create a snes with the following KSP settings:
ksp_cpr = {
           'ksp_type': 'fgmres',
           'ksp_max_it': 20,  # ECLIPSE and IX default: 20
           'pc_type': 'composite',
           'pc_composite_type': 'multiplicative',
           'pc_composite_pcs': 'python,ilu',
           'cpr_stage1_pc_type': 'hypre',
           'cpr_stage1_pc_hypre_type': 'boomeramg'
          }

2. Find the index sets for your subspaces, p_is, s_is

3. Create instance of CPR and attach to subPC:
subpc = snes.ksp.pc.getCompositePC(0)
cpr_stage1 = CPRStage1(snes, s_is, p_is, useTrueImpes)
subpc.setPythonContext(cpr_stage1)

4. Run solver
'''

from petsc4py import PETSc

class CPRStage1(object):
    '''
    This is the inner stage solver for CPR the way it is used described
    in Max and Klaus' thesis.
    '''
    def __init__(self, snes, s_is, p_is, useTrueImpes, ops_prefix=''):
        self.snes = snes

        self.useTrueImpes = useTrueImpes
        self.s_is_full, self.p_is_full = (s_is, p_is)

        self.ops_prefix = ops_prefix

        if useTrueImpes:
            self.AssT = PETSc.Mat()

    def construct_is(self):
        snes = self.snes
        if snes.getType() not in ('vinewtonrsls'):
            s_is, p_is = (self.s_is_full, self.p_is_full)
            self.s_is, self.p_is = (s_is, p_is)
            self.rows, self.cols = ([s_is, s_is, p_is, p_is],
                                    [s_is, p_is, s_is, p_is])
        else:
            inact = snes.getVIInactiveSet()
            fields = [self.s_is_full, self.p_is_full]

            # OK. Suppose you have a 6x6 matrix and the field splits are [1, 2, 3, 4]; [5, 6].
            # Suppose further that the inactive indices are [2, 3, 4, 5]. Then what we
            # want to produce for the fieldsplit of the reduced problem is [1, 2, 3]; [4].
            # In other words, we add the *index* of the inactive dof to the reduced split
            # if the inactive dof is in the full split.

            # We also need the offset of how many dofs all earlier processes own.
            from mpi4py import MPI as MPI4
            offset = MPI4.COMM_WORLD.exscan(inact.getLocalSize())
            if offset is None: offset = 0

            fsets = [set(field.getIndices()) for field in fields]
            inact_fieldsplit_is = {}
            for (i, field) in enumerate(fields):
                inact_fieldsplit_is[i] = []

            for (i, idx) in enumerate(inact.getIndices()):
                for (j, fset) in enumerate(fsets):
                    if idx in fset:
                        inact_fieldsplit_is[j].append(offset + i)
                        break

            inact_input = []
            for j in inact_fieldsplit_is:
                iset = PETSc.IS().createGeneral(inact_fieldsplit_is[j])
                inact_input.append(iset)

            s_is, p_is = inact_input
            self.s_is, self.p_is = (s_is, p_is)
            self.rows, self.cols = ([s_is, s_is, p_is, p_is],
                                    [s_is, p_is, s_is, p_is])

    def setUp(self, pc):
        self.construct_is()

        A, P = pc.getOperators()

        Ass, Asp, Aps, App = (A.createSubMatrix(self.rows[i], self.cols[i])
                              for i in range(4))
        with PETSc.Log().Event('Dss inversion') as event:
            if self.useTrueImpes is False:
                invdiag = Ass.getDiagonal()
            else:
                # TODO: Is it colsum or rowsum?
                # - If colsum: Use 1^T*Ass
                # - If rowsum: Use Ass*1
                Ass.transpose(out=self.AssT)
                invdiag = self.AssT.getRowSum()

            invdiag.reciprocal()
            invDss = PETSc.Mat().create()
            invDss.setSizes(Ass.getSizes())
            invDss.setUp()
            invDss.setDiagonal(invdiag)

        self.apsinvdss = Aps.matMult(invDss)
        Atildepp = App - self.apsinvdss.matMult(Asp)

        pc_schur = PETSc.PC().create()
        pc_schur.setType("hypre")
        pc_schur.setOptionsPrefix(self.ops_prefix + "cpr_stage1_")
        pc_schur.setFromOptions()
        pc_schur.setOperators(Atildepp)
        pc_schur.setUp()
        self.pc_schur = pc_schur

        psize = self.p_is.getLocalSize()
        # Vector for storing preconditioned pressure-vector
        self.workVec = PETSc.Vec().create()
        self.workVec.setSizes(psize)
        self.workVec.setUp()


    def apply(self, pc, x, y):
        # x is the input Vec to this preconditioner
        # y is the output Vec, P^{-1} x

        x_s = x.getSubVector(self.s_is)
        x_p = x.getSubVector(self.p_is)
        y_p = y.getSubVector(self.p_is)
        r_p = x_p.copy()
        self.apsinvdss.mult(x_s, self.workVec)

        r_p.axpy(-1.0, self.workVec)  # x_p - Aps*inv(Dss)*x_s

        self.pc_schur.apply(r_p, y_p)
