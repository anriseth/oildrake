import firedrake
from threephase import ThreePhase
import numpy as np
import os
import utils


class SPE10(ThreePhase):
    _Delta_x = 20; _Delta_y = 10; _Delta_z = 2
    _Nx = 60; _Ny = 220; _Nz = 85
    #_Nx = 40; _Ny = 120; _Nz = 85
    _kx_file = os.path.join(os.path.dirname(__file__), 'reservoir_input/spe10_kx.npy')
    _ky_file = os.path.join(os.path.dirname(__file__), 'reservoir_input/spe10_ky.npy')
    _kz_file = os.path.join(os.path.dirname(__file__), 'reservoir_input/spe10_kz.npy')
    _po_file = os.path.join(os.path.dirname(__file__), 'reservoir_input/spe10_po.npy')

    _csv = True
    _csv_eclijk = [[20,40,27], [20,40,40], [20,40,58],
                   [45,90,64], [3,7,24], [10, 150, 77],
                   [50, 150, 77], [10, 150, 76]]
    _csv_dofs = utils.ecl2dofs(_csv_eclijk, _Ny, _Nz)

    def __init__(self):
        self._name = type(self).__name__

        self._mesh = self._generate_mesh()
        layer1 = 28.0/85.0*self._Nz*self._Delta_z
        layer2 = 57.0/85.0*self._Nz*self._Delta_z
        self._ICexpr = self._ic_inversion(layer1=layer1, layer2=layer2)

        self.comm = self._mesh.comm
        self.__init_function_space__()
        self.__init_reservoir_fields__()
        self.__init_solver_params__()

        # Timestepping
        self.dt = firedrake.Constant(0.1)
        #self.dtmax = 30.0
        self.T = 3*365.0
        self.adaptive_dt = True

        self._injectors = []
        self._producers = []
        # Set up variational form to self.F
        self.__init_variational_form__()

        # datafiles for saving progress
        self.__init_datafiles__()

    def __init_reservoir_fields__(self):
        print "START: Read in reservoir fields"
        Delta = np.array([self._Delta_x, self._Delta_y, self._Delta_z])
        Vvec = firedrake.VectorFunctionSpace(self._mesh, "DG", 0)
        coords = firedrake.project(self._mesh.coordinates, Vvec).dat.data
        kx_array = np.load(self._kx_file)
        ky_array = np.load(self._ky_file)
        kz_array = np.load(self._kz_file)
        po_array = np.load(self._po_file)

        self.k_x = firedrake.Function(self._V)
        self.k_y = firedrake.Function(self._V)
        self.k_z = firedrake.Function(self._V)
        self.varphi = firedrake.Function(self._V)

        def coords2ijk(x, y, z, data_array):
            i = np.floor(x / Delta[0]).astype(int)
            j = np.floor(y / Delta[1]).astype(int)
            k = np.floor(z / Delta[2]).astype(int)
            return data_array[i, j, k]
        coords2ijk = np.vectorize(coords2ijk, excluded=['data_array'])

        self.k_x.dat.data[...] = coords2ijk(coords[:, 0], coords[:, 1],
                                            coords[:, 2], data_array=kx_array)
        self.k_y.dat.data[...] = coords2ijk(coords[:, 0], coords[:, 1],
                                            coords[:, 2], data_array=ky_array)
        self.k_z.dat.data[...] = coords2ijk(coords[:, 0], coords[:, 1],
                                            coords[:, 2], data_array=kz_array)
        self.varphi.dat.data[...] = coords2ijk(coords[:, 0], coords[:, 1],
                                               coords[:, 2], data_array=po_array)
        print "END: Read in reservoir fields"
