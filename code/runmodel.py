import petsc4py, sys
petsc4py.init(sys.argv)

from oildrake.threecell import ThreeCell as Model
model = Model()

model.init_solver()

test = False
#test = True

if test:
    model.w_next.interpolate(model._ICexpr)
    model.w_old.interpolate(model._ICexpr)
    model.solver.solve()
else:
    print("Run %s solver with T = %f, dtmax = %.1f"
          % (model._name, model.T, model.dtmax))

    with petsc4py.PETSc.Log().Stage('Solvecall') as stage:
        model.solve()

    print "Finished solver in %d timesteps" % model.timestep
