#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'ERROR: Need to supply python file'
    exit 0
fi

PYFILE=$1
PYNAME="${PYFILE%.*}"

source solversettings.sh

DIR=log
NAME="$PYNAME"
FILE="$DIR/$NAME"_lag

VERBOSE="-od_snes_monitor -od_ksp_converged_reason"

#BTFILE="$FILE"_bt
#L2FILE="$FILE"_l2

echo Writing stuff to "$FILE""_*" ...

EXTRAPARAMS="${@:3}"
echo Using extra parameters $EXTRAPARAMS
echo

## Preconditioner lagging
echo
echo "#############################################"
echo "     Run linesearch basic cases            "
echo "#############################################"
echo

for Nlag in 2 1 3; do
    # TODO: make this into a function?

    if [[ "$2" = "skip1" && $Nlag -eq 1 ]] ; then
        echo
        echo "INFO: Skipping non-lagging case"
        echo
        continue
    fi

    echo
    echo "#############################################"
    echo "     Run preconditioner lagging: $Nlag      "
    echo "#############################################"
    echo
    OUTNAME="$FILE"_"$Nlag"
    LOG="-log_summary $OUTNAME.log -log_view ascii:$OUTNAME.py:ascii_info_detail -options_left"
    python "$PYFILE" nocsv $settings_cpr_1amg \
        -od_snes_lag_preconditioner $Nlag \
        $VERBOSE $LOG $EXTRAPARAMS  &> "$OUTNAME".dump

    echo "Finished, sleeping for 5 seconds ..."
    sleep 5
done

# echo
# echo "#############################################"
# echo "#       Run linesearch BT cases             #"
# echo "#############################################"
# echo

# for Nlag in 1 2 ; do
#     echo
#     echo "#############################################"
#     echo "#     Run preconditioner lagging: $Nlag         #"
#     echo "#############################################"
#     echo
#     OUTNAME="$BTFILE"_plag_"$Nlag"
#     LOG="-log_summary $OUTNAME.log -log_view ascii:$OUTNAME.py:ascii_info_detail -options_left"
#     python "$PYFILE" nocsv -od_snes_linesearch_type bt \
#         -od_snes_lag_preconditioner $Nlag \
#         $VERBOSE $LOG &> "$OUTNAME".dump

#     echo "Finished, sleeping for 10 seconds ..."
#     sleep 10
# done

# echo
# echo "#############################################"
# echo "#       Run linesearch L2 cases             #"
# echo "#############################################"
# echo

# for Nlag in 1 2 ; do
#     echo
#     echo "#############################################"
#     echo "#     Run preconditioner lagging: $Nlag         #"
#     echo "#############################################"
#     echo
#     OUTNAME="$L2FILE"_plag_"$Nlag"
#     LOG="-log_summary $OUTNAME.log -log_view ascii:$OUTNAME.py:ascii_info_detail -options_left"
#     python "$PYFILE" nocsv -od_snes_linesearch_type l2 \
#         -od_snes_lag_preconditioner $Nlag \
#         $VERBOSE $LOG &> "$OUTNAME".dump

#     echo "Finished, sleeping for 10 seconds ..."
#     sleep 10
# done


# for Nlag in 3 5 7; do
#     echo
#     echo "#############################################"
#     echo "#         Run LBFGS lagging: $Nlag             #"
#     echo "#############################################"
#     echo
#     OUTNAME="$BASICFILE"_lbfgs_"$Nlag"
#     LOG="-log_summary $OUTNAME.log -log_view ascii:$OUTNAME.py:ascii_info_detail"
#     QN="-od_snes_type qn -od_snes_qn_m $Nlag \
#         -od_snes_qn_type lbfgs -od_snes_qn_scale_type jacobian \
#         -od_snes_qn_restart_type periodic"

#     python "$PYFILE" nocsv -od_snes_linesearch_type basic \
#         $QN $VERBOSE $LOG  &> "$OUTNAME".dump
# done

# for Nlag in 3 5 7; do
#     echo
#     echo "#############################################"
#     echo "#         Run Broyden lagging: $Nlag             #"
#     echo "#############################################"
#     echo
#     OUTNAME="$BASICFILE"_broyden_"$Nlag"

#     LOG="-log_summary $OUTNAME.log -log_view ascii:$OUTNAME.py:ascii_info_detail"
#     QN="-od_snes_type qn -od_snes_qn_m $Nlag \
#         -od_snes_qn_type broyden -od_snes_qn_scale_type jacobian \
#         -od_snes_qn_restart_type periodic"

#     python "$PYFILE" nocsv -od_snes_linesearch_type basic \
#         $QN $VERBOSE $LOG  &> "$OUTNAME".dump
# done


MAILBODY="Finished running $@"
echo $MAILBODY | mailx -s "$0 finished" riseth@maths.ox.ac.uk
