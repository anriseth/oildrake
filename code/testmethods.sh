#!/bin/bash

FILE=$1

source solversettings.sh

echo
echo "*******************************************************"
echo "*           RUNNING COMPOSITE 1AMG                    *"
echo "*******************************************************"
echo

echo "Passing the following command line parameters:"
echo $settings_cpr_1amg
echo

python "$FILE" $settings_cpr_1amg \
    -log_summary $log_cpr_1amg &> $file_cpr_composite_1amg



echo
echo "*******************************************************"
echo "*               RUNNING GMRES+ILU                     *"
echo "*******************************************************"
echo

echo "Passing the following command line parameters:"
echo $settings_gmres_ilu
echo

python "$FILE" $settings_gmres_ilu \
    -log_summary $log_gmres_ilu &> $file_gmres_ilu


echo
echo "*******************************************************"
echo "*       RUNNING COMPOSITE $Niter GMRES+ILU                *"
echo "*******************************************************"
echo


echo "Passing the following command line parameters:"
echo $settings_cpr_gmres_ilu
echo

python "$FILE" $settings_cpr_gmres_ilu \
    -log_summary $log_cpr_gmres_ilu &> $file_cpr_composite_gmres_ilu
