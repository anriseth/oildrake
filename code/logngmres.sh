#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'ERROR: Need to supply python file'
    exit 0
fi

PYFILE=$1
PYNAME="${PYFILE%.*}"
source solversettings.sh

DIR=log
NAME="$PYNAME"
FILE="$DIR/$NAME"
FILENGMRES="$FILE"_ngmres
FILELS="$FILENGMRES"_ls

VERBOSE="-od_snes_monitor -od_ksp_converged_reason -options_left"

LOGNGMRES="-log_summary $FILENGMRES.log -log_view ascii:$FILENGMRES.py:ascii_info_detail"
LOGLS="-log_summary $FILELS.log -log_view ascii:$FILELS.py:ascii_info_detail"
NGMRESMONITOR="-od_snes_ngmres_monitor"

echo Writing stuff to "$FILE""_*" ...

EXTRAPARAMS="${@:3}"
echo Using extra parameters $EXTRAPARAMS
echo

echo
echo "#############################################"
echo "              Run NGMRES -R LS               "
echo "#############################################"
echo


python "$PYFILE" nocsv $settings_cpr_1amg \
    $settings_ngmres_npc \
    -od_snes_ngmres_restart_fm_rise \
    $NPCMONITOR $NGMRESMONITOR \
    $VERBOSE $LOGNGMRES $EXTRAPARAMS &> "$FILENGMRES".dump


if [[ "$2" != "nols" ]] ; then
    echo
    echo "#############################################"
    echo "              Run Basic                      "
    echo "#############################################"
    echo

    python "$PYFILE" nocsv $settings_cpr_1amg \
        $VERBOSE $LOGLS $EXTRAPARAMS &> "$FILELS".dump
fi

MAILBODY="Finished running $@"
echo $MAILBODY | mailx -s "$0 finished" riseth@maths.ox.ac.uk
