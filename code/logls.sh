#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'ERROR: Need to supply python file'
    exit 0
fi

PYFILE=$1
PYNAME="${PYFILE%.*}"
source solversettings.sh

DIR=log
NAME="$PYNAME"_lstest
FILE=$DIR/$NAME

VERBOSE="-od_snes_monitor -od_ksp_converged_reason"

BASICFILE="$FILE"_basic
L2FILE="$FILE"_l2
BTFILE="$FILE"_bt
NLEQERRFILE="$FILE"_nleqerr

LOGBASIC="-log_summary $BASICFILE.log -log_view ascii:$BASICFILE.py:ascii_info_detail"
LOGL2="-log_summary $L2FILE.log -log_view ascii:$L2FILE.py:ascii_info_detail"
LOGBT="-log_summary $BTFILE.log -log_view ascii:$BTFILE.py:ascii_info_detail"
LOGNLEQERR="-log_summary $NLEQERRFILE.log -log_view ascii:$NLEQERRFILE.py:ascii_info_detail"

echo Writing stuff to "$FILE""_*" ...

echo
echo "#############################################"
echo "#            Run basic linesearch           #"
echo "#############################################"
echo

python "$PYFILE" $settings_cpr_1amg -od_snes_linesearch_type basic \
    $VERBOSE $LOGBASIC &> "$BASICFILE".dump

echo
echo "#############################################"
echo "#              Run bt linesearch            #"
echo "#############################################"
echo

python "$PYFILE" -od_snes_linesearch_type bt $settings_cpr_1amg \
    $VERBOSE $LOGBT &> "$BTFILE".dump


echo
echo "#############################################"
echo "#            Run l2 linesearch              #"
echo "#############################################"
echo

python "$PYFILE" -od_snes_linesearch_type l2 $settings_cpr_1amg \
    $VERBOSE $LOGL2 &> "$L2FILE".dump


# echo
# echo "#############################################"
# echo "#              Run cp linesearch            #"
# echo "#############################################"
# echo

# python "$PYFILE" -od_snes_linesearch_type cp \
#     $settings_cpr_1amg \
#     -log_summary "$FILE"_cp.log &> "$FILE"_cp.dump


echo
echo "#############################################"
echo "#         Run nleqerr linesearch            #"
echo "#############################################"
echo

python "$PYFILE" -od_snes_linesearch_type nleqerr $settings_cpr_1amg \
    $VERBOSE $LOGNLEQERR &> "$NLEQERRFILE".dump



MAILBODY="Finished running $@"
echo $MAILBODY | mailx -s "$0 finished" riseth@maths.ox.ac.uk
