#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'ERROR: Need to supply python file'
    exit 0
fi

PYFILE=$1
PYNAME="${PYFILE%.*}"
source solversettings.sh

DIR=log
NAME="$PYNAME"
FILE="$DIR/$NAME"

VERBOSE="-od_snes_monitor -od_ksp_converged_reason"

LOG="-log_summary $FILE.log -log_view ascii:$FILE.py:ascii_info_detail"

echo Writing stuff to "$FILE""_*" ...

echo
echo "#############################################"
echo "#            Run basic linesearch           #"
echo "#############################################"
echo

python "$PYFILE" nocsv $settings_cpr_1amg -od_snes_linesearch_type basic \
        $VERBOSE $LOG &> "$FILE".dump



MAILBODY="Finished running $@"
echo $MAILBODY | mailx -s "$0 finished" riseth@maths.ox.ac.uk
