import sys
from firedrake import *
from oildrake.hetero_inversion import HeteroInversion
from oildrake.hetero_inversion_big import HeteroInversionBig
from oildrake.hetero_inversion_100k import HeteroInversion100k
from oildrake.threecell import ThreeCell
from oildrake.spe10_small import SPE10Small
from oildrake.spe10 import SPE10

modelname = str(sys.argv[1]).lower()

if modelname == "heteroinversion":
    model = HeteroInversion()
elif modelname == "heteroinversionbig":
    model = HeteroInversionBig()
elif modelname == "heteroinversion100k":
    model = HeteroInversion100k()
elif modelname == "spe10small":
    model = SPE10Small()
elif modelname == "spe10":
    model = SPE10()
elif modelname == "threecell":
    model = ThreeCell()
else:
    raise Exception("Cant't recognize modelname %s" % modelname)


kxfile = File("data/kx-%s.pvd" % model._name)
kxfile << model.k_x
