import petsc4py, sys
petsc4py.init(sys.argv)
#import firedrake
from oildrake.spe10_small import SPE10Small
model = SPE10Small()

model.init_solver()

model.csv = 'nocsv' not in sys.argv
model.pvd = 'pvd' in sys.argv
test = 'test' in sys.argv

# model.DSmax = 0.4
# firedrake.info_red("Setting DSmax to %.2f" % model.DSmax)

if test:
    model.w_next.interpolate(model._ICexpr)
    model.w_old.interpolate(model._ICexpr)
    model.solver.solve()
else:
    print("\nRun %s solver with T = %f, dtmax = %.1f, DSmax = %.2f\n"
          % (model._name, model.T, model.dtmax, model.DSmax))

    with petsc4py.PETSc.Log().Stage('Solvecall') as stage:
        model.solve()

    print("\nFinished solver in %s timesteps\n") % model.timestep
    print("\nYou can plot the result with 'python plotresults.py spe10small'")
