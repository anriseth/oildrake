'''
Plot the newton convergence curves
'''

import sys, os
import numpy as np
import matplotlib.pyplot as plt


def plotresiduals(dump, fname):
    residuals = np.split(dump, np.where(dump[:, 0] == 0)[0][1:])
    residuals = [step.T for step in residuals]

    plt.figure()
    for r in residuals:
        #if len(r[0]) < 6 and np.random.rand() < 0.7: # For "pretty" plotting
        #    continue
        plt.semilogy(r[0], r[1])

    plt.title(fname)
    plt.ylabel('Residual norm', fontsize=20)
    plt.xlabel('Newton iteration', fontsize=20)
    outname = ".".join([os.path.splitext(fname)[0], 'pdf'])
    # outname = ".".join([os.path.splitext(fname)[0], 'png'])
    # plt.savefig(outname, transparent=True, dpi=200)
    # plt.semilogx()
    plt.savefig(outname)


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print "ERROR: need to supply file"
        sys.exit(1)

    print "****************************************"
    print "REMEMBER TO RUN snesmonitor2csv.sh FIRST"
    print "****************************************"

    for fname in sys.argv[1:]:
        if not os.path.isfile(fname):
            print "WARNING, %s is not a file" % fname
            continue

        dump = np.loadtxt(fname)
        plotresiduals(dump, fname)
    plt.show()
