'''
Plot the newton convergence curves
'''

import sys, os
import numpy as np
import matplotlib.pyplot as plt

def plotresiduals(dump, fname):
    residuals = np.split(dump, np.where(dump[:, 0] == 0)[0][1:])
    residuals = [step.T for step in residuals]

    plt.figure()
    for r in residuals:
        plt.semilogy(r[0], r[1])

    plt.title(fname)
    plt.ylabel('Residual norm')
    plt.xlabel('Newton iteration')
    outname = ".".join([os.path.splitext(fname)[0], 'pdf'])
    plt.savefig(outname)


def getrow(fname):
    fname = os.path.splitext(fname)[0]
    imlog = __import__(fname)
    statsdic = imlog.Stages["Solvecall"]
    runkeys = ("SNESSolve", "KSPSolve", "KSPGMRESOrthog",
               "PCSetUp", "SNESFunctionEval")
    timekeys = ("SNESSolve", "KSPSolve",
                "PCSetUp", "SNESFunctionEval", "SNESJacobianEval")
    statsdic["PCSetUp"][0]["count"] = int(statsdic["PCSetUp"][0]["count"]/3)
    runs = "&".join([str(statsdic[key][0]["count"]) for key in runkeys])
    runs = "&".join([fname, runs])
    timings = "&".join([str(round(statsdic[key][0]["time"],1)) for key in timekeys])
    timings = "&".join([fname, timings])
    return (runs, timings)

def gentable(fnames):
    rows = [getrow(fname) for fname in fnames]
    runs = [row[0] for row in rows]
    timings = [row[1] for row in rows]

    runheadings = "&".join(["Test", "Timesteps", "Nonlinears", "Linears",
                            "PCSetUps", "Functions"])
    timeheadings = "&".join(["Test", "Total", "Linear", "PCSetUp",
                             "Function", "Jacobian"])
    with open("runs.tex", "w") as out:
        out.write(runheadings + "\\\\ \n\\toprule\n" + "\\\\ \n".join([run for run in runs]) + "\n")
    with open("timings.tex", "w") as out:
        out.write(timeheadings + "\\\\ \n\\toprule\n" + "\\\\ \n".join([time for time in timings]) + "\n")

if __name__ == "__main__":

    if len(sys.argv) < 2:
        print "ERROR: need to supply file"
        sys.exit(1)

    for fname in sys.argv[1:]:
        if not os.path.isfile(fname):
            print "ERROR, %s is not a file" % fname
            sys.exit(1)

    gentable(sys.argv[1:])
