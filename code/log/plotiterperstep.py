# coding: utf-8
# TODO: generalise fname
'''
Plot the newton convergence curves
'''
import sys, os
import numpy as np
import matplotlib.pyplot as plt


def plotitersteps(ax, arr, name):
    ax.plot(arr[:, 0], arr[:, 1], marker='.')
    ax.set_xlim([0, arr[:, 0].max()])
    ax.set_ylabel('Newton iterations')
    ax.set_title(name)

if __name__ == "__main__":

    # if len(sys.argv) < 2:
    #     print "ERROR: need to supply file"
    #     sys.exit(1)

    print "*******************************************"
    print "*REMEMBER TO RUN awk lines from dump FIRST*"
    print "*******************************************"

    fname1 = 'testhetero_iterpertime_03.csv'
    fname2 = 'testspe10_iterpertime_03.csv'

    heteroarr = np.loadtxt(fname1)
    spearr = np.loadtxt(fname2)
    f, (ax1, ax2) = plt.subplots(2, 1)

    plotitersteps(ax1, heteroarr, fname1)
    plotitersteps(ax2, spearr, fname2)
    ax2.set_xlabel('Time (days)')

    maxy = heteroarr[:, 1].max()
    maxy = max(maxy, spearr[:, 1].max())
    miny = heteroarr[:, 1].min()
    miny = min(miny, spearr[:, 1].min()) - 1
    ax1.set_ylim([miny, maxy])
    ax2.set_ylim([miny, maxy])
    plt.savefig('iters_timestep_03.pdf')
    plt.show()
