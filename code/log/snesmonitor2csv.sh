#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'ERROR: Need to supply python file'
    exit 0
fi

for f in "${@:-1}"; do

    if [ ! -f "$f" ]; then
        echo "$f is not a file"
        exit 1
    fi
    fnoext="${f%.*}"
    echo $fnoext
    cat "$f" | awk '/SNES Function/ {print $1" "$5}' > "$fnoext.snesmonitor"
done
