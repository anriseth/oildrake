import sys
import numpy as np
import matplotlib.pyplot as plt
from oildrake.utils import ecl2dofs

from oildrake.hetero_inversion import HeteroInversion
from oildrake.hetero_inversion_big import HeteroInversionBig
from oildrake.hetero_inversion_100k import HeteroInversion100k
from oildrake.threecell import ThreeCell
from oildrake.spe10_small import SPE10Small
from oildrake.spe10 import SPE10

modelname = str(sys.argv[1]).lower()

if modelname == "heteroinversion":
    # HeteroInversion, based on 24x24x24
    #eclipseids = np.array([[8,8,8], [8,8,11], [8,8,15],
                           #[20,15,13], [3,22,18]])
    model = HeteroInversion()
elif modelname == "heteroinversionbig":
    model = HeteroInversionBig()
elif modelname == "heteroinversion100k":
    model = HeteroInversion100k()
elif modelname == "spe10small":
    model = SPE10Small()
    #eclipseids = np.array([[4,4,3], [4,4,5], [4,4,8],
                           #[8,1,9], [3,7,2]])
elif modelname == "spe10":
    model = SPE10()
    #eclipseids = np.array([[20,40,27], [20,40,40], [20,40,58],
                           #[45,90,64], [3,7,24]])
elif modelname == "threecell":
    model = ThreeCell()
    #eclipseids = np.array([[1,1,1], [1,1,2], [1,1,3]])
else:
    raise Exception("Don't have eclipseids for model %s" % modelname)


model.plotresults()

#ny, nz = (model._Ny, model._Nz)
#dofids = np.apply_along_axis(ecl2dofs, 1, eclipseids, ny=ny, nz=nz)
#tarr, oarr, garr, warr, parr = model.values(cells=dofids)

# f, axarr = plt.subplots(2, 2, sharex='col')#, sharey='row')

# l00 = axarr[0, 0].plot(tarr, parr)
# axarr[0, 1].plot(tarr, oarr)
# axarr[1, 0].plot(tarr, garr)
# axarr[1, 1].plot(tarr, warr)

# axarr[0, 0].set_ylabel('Pressure (bar)')
# axarr[0, 1].set_ylabel('Oil saturation')
# axarr[1, 0].set_ylabel('Gas saturation')
# axarr[1, 1].set_ylabel('Water saturation')
# axarr[1, 1].set_xlabel('Time (days)')
# axarr[1, 0].set_xlabel('Time (days)')

# axarr[1, 0].set_xlim([tarr.min(), tarr.max()])
# axarr[1, 1].set_xlim([tarr.min(), tarr.max()])
# axarr[0, 1].set_ylim([0, 1])
# axarr[1, 0].set_ylim([0, 1])
# axarr[1, 1].set_ylim([0, 1])

# legends = [",".join(map(str, eclipseids[i]))
#            for i in range(eclipseids.shape[0])]
# legends = [str(eclipseids[i])
#            for i in range(eclipseids.shape[0])]

# f.legend(l00, legends,
#          loc='upper right', title='ECLIPSE ijk')
# plt.suptitle("%s solution for selected cells" % model._name)
# plt.show()
