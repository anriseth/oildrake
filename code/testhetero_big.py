import petsc4py, sys
petsc4py.init(sys.argv)

from oildrake.hetero_inversion_big import HeteroInversionBig as Model
model = Model()

model.init_solver()

model.csv = 'nocsv' not in sys.argv
model.pvd = 'pvd' in sys.argv
test = 'test' in sys.argv

if test:
    model.w_next.interpolate(model._ICexpr)
    model.w_old.interpolate(model._ICexpr)
    with petsc4py.PETSc.Log().Stage('Solvecall') as stage:
        model.solver.solve()
else:
    print("Run %s solver with T = %f, dtmax = %.1f"
          % (model._name, model.T, model.dtmax))

    with petsc4py.PETSc.Log().Stage('Solvecall') as stage:
        model.solve()

    print "Finished solver in %d timesteps" % model.timestep
