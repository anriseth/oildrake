import petsc4py, sys
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
petsc4py.init(sys.argv)
from petsc4py import PETSc
from firedrake import *

parameters["matnest"] = False

from oildrake.spe10_small import SPE10Small as Model
#from oildrake.wellcase import WellCase as Model
#from oildrake.spe10 import SPE10 as Model

model = Model()
model.init_solver()

W = model._W
F = model.F
w_next = model.w_next

fdofs = W.dof_dset.field_ises
s_is = PETSc.IS().createGeneral(
    np.concatenate([iset.indices for iset in fdofs[:-1]]))
p_is = fdofs[-1]
J = derivative(F, w_next)


rows, cols = ([s_is, s_is, p_is, p_is],
              [s_is, p_is, s_is, p_is])


test = True
if test:
    model.w_next.interpolate(model._ICexpr)
    model.w_old.interpolate(model._ICexpr)
    Jmat0 = assemble(J).M.values
    model.solver.solve()
    Jmat1 = assemble(J).M.values
    plt.figure(1)
    plt.title('Jacobian at t = 0.0')
    plt.spy(Jmat0, precision=1e-3, marker='.', markersize=1)

    plt.figure(2)
    plt.title('Jacobian at t = %.2f' % float(model.dt))
    plt.spy(Jmat1, precision=1e-3, marker='.', markersize=1)
    plt.yticks([1000, 2000, 3000, 4000])
    plt.xticks([1000, 2000, 3000, 4000])
    # plt.savefig('jacobian.eps')
    plt.show()



    #with petsc4py.PETSc.Log().Stage('Solvecall') as stage:
    #    model.solver.solve()
else:
    with petsc4py.PETSc.Log().Stage('Solvecall') as stage:
        model.solve()
