import numpy as np
import matplotlib.pyplot as plt


def load_variables(folder, extension, skiprows=0):
    t = np.loadtxt('%stime-%s.csv' % (folder, extension))
    o = np.loadtxt('%soil-%s.csv' % (folder, extension),
                   delimiter=',', skiprows=skiprows)
    g = np.loadtxt('%sgas-%s.csv' % (folder, extension),
                   delimiter=',', skiprows=skiprows)
    w = np.loadtxt('%swater-%s.csv' % (folder, extension),
                   delimiter=',', skiprows=skiprows)
    p = np.loadtxt('%spressure-%s.csv' % (folder, extension),
                   delimiter=',', skiprows=skiprows)

    return (t, o, g, w, p)

# ECLIPSE data
folder = './'
eclextension = 'eclipse'
odextension = 'SPE10Small'
eclipseids = np.array([[4,4,3], [4,4,5], [4,4,8],
                       [8,1,9], [3,7,2]])

tarr, oarr, garr, warr, parr = load_variables(folder, odextension, skiprows=0)

f, axarr = plt.subplots(2, 2, sharex='col')#, sharey='row')

l00 = axarr[0, 0].plot(tarr, parr)
axarr[0, 1].plot(tarr, oarr)
axarr[1, 0].plot(tarr, garr)
axarr[1, 1].plot(tarr, warr)

axarr[0, 0].set_ylabel('Pressure (bar)')
axarr[0, 1].set_ylabel('Oil saturation')
axarr[1, 0].set_ylabel('Gas saturation')
axarr[1, 1].set_ylabel('Water saturation')
axarr[1, 1].set_xlabel('Time (days)')
axarr[1, 0].set_xlabel('Time (days)')

axarr[1, 0].set_xlim([tarr.min(), tarr.max()])
axarr[1, 1].set_xlim([tarr.min(), tarr.max()])
axarr[0, 1].set_ylim([0, 1])
axarr[1, 0].set_ylim([0, 1])
axarr[1, 1].set_ylim([0, 1])


legends = [str(eclipseids[i])
           for i in range(len(eclipseids))]
f.legend(l00, legends,
         loc='center right', title='ECLIPSE ijk')

plt.suptitle("%s solution for selected cells" % 'SPE10Small')
plt.savefig('solution_oildrake.eps')

te, oe, ge, we, pe = load_variables(folder, eclextension, skiprows=1)
earr = (pe, oe, ge, we)

for i, ax in enumerate(axarr.flatten()):
    ax.plot(te, earr[i], 'k--')

# TODO: add a single ECLISPE legend?
plt.suptitle("%s solution comparison for selected cells" % 'SPE10Small')
plt.savefig('solution_comparison.eps')
plt.show()
