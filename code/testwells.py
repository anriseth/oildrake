import petsc4py, sys
petsc4py.init(sys.argv)
from firedrake import *
from oildrake.wellcase import WellCase as Model
#from oildrake.threecell import ThreeCell as Model
#from oildrake.spe10 import SPE10 as Model
#from oildrake.spe10_small import SPE10Small as Model

model = Model()

model.init_solver()

test = True
if test:
    model.w_next.interpolate(model._ICexpr)
    model.w_old.interpolate(model._ICexpr)
    # F = model.F
    # print norm(assemble(F))

    # u = model.w_next
    # J = derivative(F, u)
    # A = assemble(J).M.handle
    # from slepc4py import SLEPc
    # eps = SLEPc.EPS().create()
    # eps.setOperators(A)
    # eps.solve()
    # for i in range(eps.getConverged()):
    #     print eps.getEigenvalue(i)

    solver = model.solver
    snes = solver.snes
    solver.solve()

if not test:
    print("Run %s solver with T = %.2f, dtmax = %.2f"
          % (model._name, model.T, model.dtmax))

    model.solve()

    print("\nTo plot the results, run model.plotresults()")
    if plotresults:
        model.plotresults()
