from oildrake.spe10_small import SPE10Small
import numpy as np
import matplotlib.pyplot as plt
from firedrake import *

model = SPE10Small()

#model.init_solver()
#model.solve()

print ""
print "***********************************************************************"
print "WARNING: This code has not been updated with the new ThreePhase._csv_dofs system"
print "You need to run SPE10Small with _csv_dofs=None for this code to work"
print "***********************************************************************"
print ""

# ECLIPSE (i,j,k) is (3,2,2), (5,3,5), (9,8,8)
# Python should be (2,1,8), (4,2,5), (8,7,2)
eclipseids = np.array([[3,2,2], [5,3,5], [9,8,8]])
pythonids = eclipseids - 1
pythonids[:,2] = 9 - pythonids[:,2]
dofids = model._Nz*model._Ny*pythonids[:, 0] + \
         model._Nz*pythonids[:, 1] + \
         pythonids[:, 2]

Delta = np.array([model._Delta_x, model._Delta_y, model._Delta_z])

tarr, oarr, garr, warr, parr = model.values()
oarr = oarr[:, dofids]
garr = garr[:, dofids]
warr = warr[:, dofids]
parr = parr[:, dofids]

# ECLIPSE data
folder = './data/spe10_small_eclipse/'

te = np.loadtxt(folder + 'time' + '-eclipse.csv')
oe = np.loadtxt(folder + 'oil' + '-eclipse.csv', delimiter=',', skiprows=1)
ge = np.loadtxt(folder + 'gas' + '-eclipse.csv', delimiter=',', skiprows=1)
we = np.loadtxt(folder + 'water' + '-eclipse.csv', delimiter=',', skiprows=1)
pe = np.loadtxt(folder + 'pressure' + '-eclipse.csv', delimiter=',', skiprows=1)

plt.figure(1); plt.clf()
plt.plot(tarr, oarr)
plt.plot(te, oe, 'k')
plt.xlabel('Time (days)')
plt.ylabel('Oil saturation')

plt.figure(2); plt.clf()
plt.plot(tarr, garr)
plt.plot(te, ge, 'k')
plt.xlabel('Time (days)')
plt.ylabel('Gas saturation')

plt.figure(3); plt.clf()
plt.plot(tarr, warr)
plt.plot(te, we, 'k')
plt.xlabel('Time (days)')
plt.ylabel('Water saturation')

plt.figure(4); plt.clf()
plt.plot(tarr, parr)
plt.plot(te, pe, 'k')
plt.xlabel('Time (days)')
plt.ylabel('Pressure (bar)')

plt.show()
