import petsc4py, sys
petsc4py.init(sys.argv)
from oildrake.spe10 import SPE10
from firedrake import *
op2.init(log_level=WARNING)
model = SPE10()

model.init_solver()

model.csv = 'nocsv' not in sys.argv
model.pvd = 'pvd' in sys.argv
test = 'test' in sys.argv

if test:
    model.w_next.interpolate(model._ICexpr)
    model.w_old.interpolate(model._ICexpr)
    model.solver.solve()

else:
    print("Run %s solver with T = %f, dtmax = %.1f"
          % (model._name, model.T, model.dtmax))

    with petsc4py.PETSc.Log().Stage('Solvecall') as stage:
        model.solve()

    print("\nFinished solver in %s timesteps\n") % model.timestep
    print("\nTo check results against ECLIPSE, run checkspe.py")
