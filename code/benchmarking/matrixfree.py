import petsc4py, sys, os
os.path.join(os.path.dirname(__file__), os.pardir)
petsc4py.init(sys.argv)
from firedrake import *
#from oildrake.spe10_small import SPE10Small as Model
from oildrake.spe10 import SPE10 as Model
#from oildrake.wellcase import WellCase as Model

from pyop2.profiling import timed_region, summary
parameters["pyop2_options"]["lazy_evaluation"] = False
parameters["pyop2_options"]["profiling"] = True

model = Model()

W = model._W
F = model.F
u = model.w_next

out = Function(W)
J = derivative(F, u)
Jv = action(J, u)

with timed_region('Assemble J'):
    A = assemble(J)
    A.M

with timed_region('MatVec'):
    with u.dat.vec_ro as x:
        with out.dat.vec as y:
            A.M.handle.mult(x, y)

with timed_region('Matrix free'):
    Av = assemble(Jv)

summary()
