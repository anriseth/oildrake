import petsc4py, sys
import numpy as np
petsc4py.init(sys.argv)
from petsc4py import PETSc
from firedrake import *

#from oildrake.spe10_small import SPE10Small as Model
from oildrake.spe10 import SPE10 as Model

model = Model()
model.init_solver()

W = model._W
F = model.F
w_next = model.w_next

fdofs = W.dof_dset.field_ises
s_is = PETSc.IS().createGeneral(
    np.concatenate([iset.indices for iset in fdofs[:-1]]))
p_is = fdofs[-1]
J = derivative(F, w_next)


rows, cols = ([s_is, s_is, p_is, p_is],
              [s_is, p_is, s_is, p_is])

# TODO:
# - Make S into a mult object S = App + Asp*invDss
# - Implement True-Impes. Should just be colsum of A_sp?

class CPRInner(object):
    def __init__(self, s_is, p_is):
        self.rows, self.cols = ([s_is, s_is, p_is, p_is],
                                [s_is, p_is, s_is, p_is])

        psize = p_is.getLocalSize()
        self.s_is = s_is
        self.p_is = p_is
        # Vector for storing preconditioned pressure-vector
        self.y_p = PETSc.Vec().create()
        self.y_p.setSizes(psize)
        self.y_p.setUp()
        self.workVec = self.y_p.duplicate()

    def setUp(self, pc):
        A, P = pc.getOperators()
        Ass, Asp, Aps, App = (A.getSubMatrix(self.rows[i], self.cols[i])
                              for i in range(4))

        invdiag = Ass.getDiagonal()
        invdiag.reciprocal()
        invDss = PETSc.Mat()
        invDss = invDss.create()
        invDss.setSizes(Ass.getSizes())
        invDss.setUp()
        invDss.setDiagonal(invdiag)

        self.workMat = Aps.matMult(invDss)
        # TODO: make this matrixfree?
        Atildepp = App - self.workMat.matMult(Asp)

        ksp_schur = PETSc.KSP()
        ksp_schur.create()
        ksp_schur.setType("preonly")
        # TODO: how do I set hypre_type?
        ksp_schur.pc.setType("hypre")
        ksp_schur.setOptionsPrefix("cpr_inner_schur_")
        ksp_schur.setFromOptions()
        ksp_schur.setOperators(Atildepp)
        ksp_schur.setUp()
        self.ksp_schur = ksp_schur

    # First step of CPR preconditioning
    def apply(self, pc, x, y):
        # x is the input Vec to this preconditioner
        # y is the output Vec, P^{-1} x

        #with x.getSubVector(self.s_is) as x_s, x.getSubVector(self.p_is) as x_p:
        x_s = x.getSubVector(self.s_is)
        x_p = x.getSubVector(self.p_is)
        self.workMat.mult(x_s, self.workVec)
        w_p = x_p.copy()
        w_p.axpy(-1.0, self.workVec) # x_p - Asp*inv(Dss)*x_s

        self.ksp_schur.solve(w_p, self.y_p)
        y.setValues(self.p_is, self.y_p)


class CPR(object):
    def __init__(self, s_is, p_is):
        self.rows, self.cols = ([s_is, s_is, p_is, p_is],
                                [s_is, p_is, s_is, p_is])

        psize = p_is.getLocalSize()
        ssize = s_is.getLocalSize()
        self.s_is = s_is
        self.p_is = p_is
        # Vector for storing preconditioned pressure-vector
        self.y_p = PETSc.Vec().create()
        self.y_p.setSizes(psize)
        self.y_p.setUp()
        self.workVec_p = self.y_p.duplicate()

        self.workVec = PETSc.Vec().create()  # Corrected residual
        self.workVec.setSizes(psize+ssize)
        self.workVec.setUp()

    def setUp(self, pc):
        A, P = pc.getOperators()
        Ass, Asp, Aps, App = (A.getSubMatrix(self.rows[i], self.cols[i])
                              for i in range(4))
        self.Asp = Asp
        self.App = App
        invdiag = Ass.getDiagonal()
        invdiag.reciprocal()
        invDss = PETSc.Mat()
        invDss = invDss.create()
        invDss.setSizes(Ass.getSizes())
        invDss.setUp()
        invDss.setDiagonal(invdiag)

        self.workMat = Aps.matMult(invDss)
        # TODO: make this matrixfree?
        Atildepp = App - self.workMat.matMult(Asp)

        ksp_schur = PETSc.KSP()
        ksp_schur.create()
        ksp_schur.setType("preonly")
        # TODO: how do I set hypre_type?
        ksp_schur.pc.setType("hypre")
        ksp_schur.setOptionsPrefix("cpr_inner_schur_")
        ksp_schur.setFromOptions()
        ksp_schur.setOperators(Atildepp)
        ksp_schur.setUp()
        self.ksp_schur = ksp_schur

        pc_ilu = PETSc.PC().create()
        pc_ilu.setType("ilu")
        pc_ilu.setOperators(A)
        pc_ilu.setUp()
        pc_ilu.setFactorLevels(0)
        self.pc_ilu = pc_ilu

    # First step of CPR preconditioning
    def apply(self, pc, x, y):
        # x is the input Vec to this preconditioner
        # y is the output Vec, P^{-1} x

        x_s = x.getSubVector(self.s_is)
        x_p = x.getSubVector(self.p_is)
        self.workMat.mult(x_s, self.workVec_p)
        w_p = x_p.copy()
        w_p.axpy(-1.0, self.workVec_p) # w_p = x_p - Asp*inv(Dss)*x_s

        self.ksp_schur.solve(w_p, self.y_p)

        workVec_s = self.workVec.getSubVector(s_is)
        workVec_p = self.workVec.getSubVector(p_is)
        self.Asp.mult(self.y_p, workVec_s)
        self.App.mult(self.y_p, workVec_p)

        rc = x.copy()
        rc.axpy(-1.0, self.workVec)  # rc = x - A*s, where s is expanded

        self.pc_ilu.apply(rc, y)

        addy = self.y_p.duplicate()
        addy = y.getValues(self.p_is)
        y.setValues(self.p_is, addy+self.y_p)


mpc = model.solver.snes.ksp.pc
if mpc.getType() == 'composite':
    pcnum = 0 if mpc.getCompositePC(0).getType() == 'python' else 1
    subpc = mpc.getCompositePC(pcnum)
    cpr_inner = CPRInner(s_is, p_is)
    subpc.setPythonContext(cpr_inner)
elif mpc.getType() == 'python':
    print "Using full cpr"
    cpr = CPR(s_is, p_is)
    mpc.setPythonContext(cpr)


test = True
test = False
if test:
    model.w_next.interpolate(model._ICexpr)
    model.w_old.interpolate(model._ICexpr)

    with petsc4py.PETSc.Log().Stage('Solvecall') as stage:
        model.solver.solve()
else:
    with petsc4py.PETSc.Log().Stage('Solvecall') as stage:
        model.solve()
