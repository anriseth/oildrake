\documentclass{beamer}
\usetheme{InFoMM}
\usepackage{todonotes}
\usepackage{booktabs}
\usepackage{algpseudocode}
\usepackage{setspace}

\author{Asbjørn Nilsen Riseth}
\title{Nonlinear solver techniques in reservoir simulation}
%\subtitle{SUBTITLE}
%\institute{INSTITUTE}
%\subject{SUBJECT}
\date{\today}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection, sectionstyle=show/show, subsectionstyle=show/show/hide]
  \end{frame}
}

\begin{document}

\frame{\titlepage}

\begin{frame}{Introduction}
  \begin{itemize}
    \item 10 week project
      \begin{itemize}
      \item Schlumberger Abingdon
      \item Oxford Mathematical Institute
      \end{itemize}
    \item Investigate nonlinear solvers for reservoir simulation
    \begin{itemize}
      \item Nonlinear GMRES: Accelerates convergence
      \item Other techniques
    \end{itemize}
  \item Implement mini reservoir simulator
    \begin{itemize}
      \item Easy to test different solver approaches
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Oildrake}

\subsection{Capabilities}
\begin{frame}{Capabilities and restrictions}
  \begin{itemize}
  \item Simplest ECLIPSE E300 configuration
  \item Immiscible three-phase
    \begin{itemize}
    \item Black oil, dry gas
    \end{itemize}
  \item Ignore capillary pressure
  \item Supported, but not used in tests
    \begin{itemize}
    \item Heterogeneous porosity
    \item Anisotropic permeability
    \item Single-connected wells
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Tools}
\begin{frame}[plain]{Building a reservoir simulator in 10 weeks}
  \only<1> {
    \begin{figure}
      \centering
      \includegraphics[width=0.8\textwidth]{../img/firedrake_toolchain.png}
    \end{figure}
  }
  \only<2>{
    Rathgeber et~al.(2015). Firedrake: automating the finite element method by composing
  abstractions.
  }
\end{frame}


\subsection{Reservoir model}
\begin{frame}{Governing equations}
 Find $p,m_\alpha$,
$\alpha=g,o,w$ such that
\begin{align*}
    \frac{\partial \phi m_\alpha}{\partial t} +
    \nabla\cdot (b_\alpha v_\alpha) &= \sigma_\alpha & \text{Mass balance}\\
    v_\alpha &= -K\frac{k_{r,\alpha}}{\mu_\alpha}\left( \nabla p -
               \rho_\alpha \mathbf g \right) & \text{Darcy flow} \\
    \sum_\alpha S_\alpha &= 1  & \text{Volume constraint}\label{eq:material_balance}\\
    \frac{\partial b_\alpha v_\alpha} {\partial n} &= 0
                                                     &\text{No
                                                       boundary flow}.
\end{align*}
\begin{itemize}
\item Solve using piecewise constant discontinuous Galerkin (FVM) and
  backward Euler
\end{itemize}
\end{frame}

\subsection{In action}


\begin{frame}{Gas inversion}
  \begin{itemize}
  \item Gravity-induced flow
  \item Highest density on top
  \end{itemize}
  \includegraphics[width=\textwidth]{../img/kx-heteroinversion.png}
  \onslide<2> {
  \begin{itemize}
  \item Video
  \end{itemize}
}
\end{frame}

\begin{frame}{Validation}
  \begin{itemize}
  \item Comparison with ECLIPSE E300
  \item Good fit
  \end{itemize}
  \includegraphics[width=\textwidth]{../img/kx-spe10small.png}
\end{frame}

\begin{frame}[plain]{Validation}
\only<1>{
  \includegraphics[width=\textwidth]{../img/solution_oildrake.eps}
}
\only<2>{
  \includegraphics[width=\textwidth]{../img/solution_comparison.eps}
}
\end{frame}

\section{Nonlinear solvers}

\subsection{Newton method and performance}

\begin{frame}{Newton method}
  \begin{itemize}
  \item Nonlinear discrete system for each timestep
    \begin{itemize}
      \item Find root of $F_n:\mathbb R^{4N}\to\mathbb R^{4N}$
    \end{itemize}
  \end{itemize}
  \begin{algorithmic}
    \Procedure{Newton}{$F, x_i$}
    \State $d = J(x_i)^{-1}F(x_i)$
    \Comment Invert Jacobian: linear system
    \State $x_{i+1} = x_i - \lambda d$
    \Comment $\lambda$ chosen using line search
    \EndProcedure
  \end{algorithmic}
\end{frame}


\begin{frame}{Newton performance}
  \begin{table}
    \begin{tabular}{llrr}
      \begin{tabular}{llrr}
        Operation & SubOp & Share & SubShare\\
        \toprule
        Linear solver & &47\% &\\
                           &PC setup & & 62\% \\
        Jacobian assembly && 41\% &\\
        Residual evaluation& & 10\%  &\\
        Projection && 2\% &
      \end{tabular}
    \end{tabular}
  \end{table}
  \begin{itemize}
  \item Linear solver share increases with problem size
  \item Differences from industry software
  \item Test problems are quite easy
    \begin{itemize}
    \item Both for linear and nonlinear solver
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[plain]{Newton performance}
  \includegraphics[width=\textwidth]{../img/testspe10_small_ngmres_ls.pdf}
\end{frame}

\subsection{Nonlinear solver techniques}
\begin{frame}{Nonlinear solver techniques}{Large decision space!}
  \begin{itemize}
    \item Quasi-Newton
      \begin{itemize}
      \item Trade more nonlinear iterations for solving
        the same matrix
      \end{itemize}
    \item NGMRES
      \begin{itemize}
      \item Trade fewer Newton steps for more residual
        evaluations
      \end{itemize}
    \item Preconditioner lagging
      \begin{itemize}
      \item  Trade fewer preconditioner
        assemblies for more Krylov iterations
      \end{itemize}
    \item Complementarity solvers
      \begin{itemize}
      \item How to deal with bounds?
      \end{itemize}
    \end{itemize}
\end{frame}

\subsection{Improvement using N-GMRES}
\begin{frame}{Nonlinear GMRES-accelerated Newton}
  \begin{itemize}
  \item Reduces number of nonlinears by about 10\%
    \begin{itemize}
    \item Even in these easy regimes
    \end{itemize}
  \item Expense: Increase function evaluations by 60\%-70\%
    \begin{itemize}
    \item Less dependent on difficulty
    \end{itemize}
  \end{itemize}

  \only<1>
  {
    \footnotesize
    \begin{table}[htdp]
      \begin{tabular}{lrrrr}
        &\multicolumn{2}{c}{Nonlinear solves}
        &\multicolumn{2}{c}{Residual evaluations}\\
        \toprule
        Test&Newton&NGMRES&Newton&NGMRES\\
        \midrule
        SPE10 &854&769&1037&1721\\
        %SPE10 &769&1721\\
        Hetero &1113&991&1349&2218\\
        %Hetero &991&2218\\
        Hetero25k &1719&1531&2095&3438\\
        %Hetero25k &1531&3438\\
        Hetero100k &2740&2479&3317&5535
        %Hetero100k &2479&5535
      \end{tabular}
    \end{table}
  }
  \only<3>
  {
    \begin{table}
    \begin{tabular}{llrr}
      \begin{tabular}{llrr}
        Operation & SubOp & Share & SubShare\\
        \toprule
        Linear solver & &43\% &\\
                           &PC setup & & 60\% \\
        Jacobian assembly && 38\% &\\
        Residual evaluation& & 16\%  &\\
        Projection && 2\% &
      \end{tabular}
    \end{tabular}
  \end{table}
  }
  \only<2>
  {
    \begin{table}
      \begin{tabular}{llrr}
        \begin{tabular}{llrr}
          Operation & SubOp & Share & SubShare\\
          \toprule
          Linear solver & &47\% &\\
                    &PC setup & & 62\% \\
          Jacobian assembly && 41\% &\\
          Residual evaluation& & 10\%  &\\
          Projection && 2\% &
        \end{tabular}
      \end{tabular}
    \end{table}
  }
\end{frame}

\begin{frame}{Nonlinear GMRES accelerated Newton}

  \begin{itemize}
  \item Take Newton step
  \item Accelerate based on previous steps
  \end{itemize}
  {
    \setstretch{1.3}
    \footnotesize
    \begin{algorithmic}
      \Procedure{NGMRES}{$F, x_i, \dots, x_{i-m+1}$}
      \State $x_i^M = M(F, x_{i-1})$ \Comment Newton step
      \State minimize $\left\|F\left( \left(
            1-\sum_{k=i-m}^{i-1}\alpha_i\right)x_i^M +
          \sum_{k=i-m}^{i-1}\alpha_k x_k \right) \right\|_2$
      \State over $\{\alpha_{i-m},\dots\alpha_{i-1}\}$
      \State $x_i^A =\left(
        1-\sum_{k=i-m}^{i-1}\alpha_i\right)x_i^M +
      \sum_{k=i-m}^{i-1}\alpha_k x_k$
      \State $x_{i+1}=x_i^A$ or $x_i^M$ if $x_i^A$ is insufficient.
      \EndProcedure
    \end{algorithmic}
  }
  \onslide<2>
  {
    \begin{itemize}
    \item Easy with PETSc:
    \end{itemize}
    {
      \small
      \quad -snes\_type ngmres -snes\_npc\_side right -npc\_snes\_type newtonls
    }

  }
\end{frame}


\begin{frame}{Summary}
  Project outcome
  \begin{itemize}
  \item Easy to test new approaches
    \begin{itemize}
    \item Large design space
    \end{itemize}
  \item NGMRES moves work to scalable subproblems
  \end{itemize}
  Follow up
  \begin{itemize}
  \item More difficult nonlinear problems
    \begin{itemize}
    \item PDE formulation, wells
    \end{itemize}
  \item Solver techniques
    \begin{itemize}
    \item Nonlinear Additive Schwarz
    \item Complementarity solvers
    \end{itemize}
  \end{itemize}
\end{frame}


\end{document}
