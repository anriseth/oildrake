# README #

Git repository for the InFoMM + Schlumberger nonlinear solver project, written by Asbjørn Nilsen Riseth in 2015.
Some notes on installation of dependencies can be found in INSTALL

## Running the code ##
I normally run code using _test*.py_ files, or for logging, through bash scripts. **The first time you run test cases, the system will do lots of compilation. This will only happen the first time**
All files and paths are relative to _code/_
Some relevant files are described underneath.

### Oildrake files
All these files are found under _code/oildrake_
Currently the threephase.py \_\_init\_\_ solution is not ideal, so I don't necessarily use it in the subclasses.

#### threephase.py
Contains the super-class for the different cases.
Should probably separate thing out more from this class.
Currently it sets lots of default values, includes the setup of the variational form,
solver, timestep loop and file writing.

Each cell is currently set to default size 20x10x2. Time is in days.

__Timestep-related constants__
- dtmax: maximum allowed timestep size. Default 50.0
- DSmax: maximum allowed predicted saturation change. For adaptive timestepping.
- dtmaxinc: maximum factor allowed to increase the timestep by. For adaptive timestepping.

#### hetero_inversion.py: HeteroInversion subclass of ThreePhase
Currently a 24x24x24 system. Generates a "smooth" permeability field using a weird truncated
fourier series and a random number generator.

Initial conditions are gas at the bottom, oil in the middle and water on top.

#### testspe10_small.py TestSPE10Small subclass of ThreePhase
A 10x10x10 system, with highly varying permeability field. Using a file from Max and Klaus' thesis

Initial conditions are gas at the bottom, oil in the middle and water on top.

### Bash scripts ###
Bash scripts are used to deal with running tests, dumping logs and _grep_- and _awk_-ing.

#### solversettings.sh
Contains _$settings\_cpr\_1amg_, which is used to pass the CPR related solver options to the PETSc solver.
That is FGMRES, PCCOMPOSITE PYTHON,ILU where PYTHON is the inner CPR stage using 1 AMG V-cycle.

__The PETSc prefix for the Firedrake solver is "od_"__

#### logmonitor.sh
_./logmonitor.sh <pythonfile>_ is used to run my _test*.py_ files and dump all logs in _log/_

I currently run _./logmonitor.sh testhetero.py_ and _./logmonitor.sh testspe10\_small.py_
to get all my output for the graphs etc.

#### log/snesmonitor2csv.sh
Takes _log/*.dump_ files and extracts the residual data to use with _log/plotresidual.py_

### Python script files

#### testhetero.py and testspe10_small.py
These show how I create model instances, set up PETSc staging and run the model solver.
Currently used with _logmonitor.sh_ to create logs etc.

#### plotresults.py
Plots the solutions for some cells. Takes the data from _data/*.csv_. You need to run the models first (and make sure that they have set _self.\_csv = True_.
Needs an argument: lower case version of the model.\_name that is defined for all the ThreePhase subclasses.

#### log/plotresidual.py
Creates one figure containing all residual vs Newton iterations from a model.solve()
To be used with _log/*.snesmonitor_ files created by _log/snesmonitor2csv.sh_.

#### log/ploriterperstep.py
Creates a plot for testhetero and testspe10\_small showing how many Newton iterations are taken for each timestep.
You need to do _cat *.dump | awk '/newdt/ {print $3" "$9} > *.csv'_ to get the correct columns from the dump.
Currently hard coded.


## TODO ##

### First priority ###
- Set up harder test cases
- Ensure the code is OK for parallel
    * Not working currently. Divergence
    * Accessing <object>.dat.data might break this
    * Is the projection_monitor okay?


### Secondary stuff ###
- Box sizes (20,10,2) in metres is a bit big. Original SPE10 uses the same in feet
- Put more functions in utils.py
- Rewrite code for injectors and/or producers

#### Potential long-term: ####
- Tell firedrake to use first (or 0th) order quadratures. If that makes a difference?
- Revise snes tolerances
    * Using atol = 1e-3, rtol = 1e-8
    * INTERSECT uses max-norm 1e-3 (on mass balance residuals)
- Eisenstat-Walker forcing
- Matrix-free methods
    * Firedrake isn't well suited for this at the moment
- Make code more clever
    * Separate out things
    * Have a "config file" for all input decisions
    * Take options on command line
- Set up SPE10
    * Redefine relative permeabilities? (S_c and S**2)
    * Implement wells (if single-connected)
- Plotting to PVD  / XDMF
    * Martin Lange is implementing hdf5 in summer 2015. Move to this
    * DG0->DG1 projection at the moment. DG0 was removed in:
    * https://github.com/firedrakeproject/firedrake/commit/e25ac3f10647a2702428e06cc484471925f360ff